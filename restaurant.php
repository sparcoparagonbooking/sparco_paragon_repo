<?php 
	$page_id=7;
	include('includes/header.php'); 
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg" />
    </div>
	<?php 
        include('includes/menu.php'); 
    ?>
    <div class="container">
    	<div class="inner_page_mainheading">
            <h1>Restaurants</h1>
            <hr>
            <p>Lorem ipsum dolor sit amet,</p>
        </div>
        <div class="row restaurants">
        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 restaurants_item">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0">
                    	<img src="images/j1.jpg" />
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4>Paragon Hotel</h4>
                        <p class="place">Place : Kozhikod</p>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit,</p>
                        <div class="view_menu"><a href="restaurant-menu.php">View Menu!</a> </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 restaurants_item">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0">
                    	<img src="images/j1.jpg" />
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4>Paragon Hotel</h4>
                        <p class="place">Place : Kozhikod</p>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit,</p>
                        <div class="view_menu"><a href="restaurant-menu.php">View Menu!</a> </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 restaurants_item">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0">
                    	<img src="images/j1.jpg" />
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4>Paragon Hotel</h4>
                        <p class="place">Place : Kozhikod</p>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. dolor sit amet, consectetur adipiscing elit,</p>
                        <div class="view_menu"><a href="restaurant-menu.php">View Menu!</a> </div>

                    </div>
                </div>
            </div>
        </div>
    
</div>

<?php 
	include('includes/footer.php'); 
?>