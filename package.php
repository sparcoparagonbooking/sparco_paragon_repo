<?php 
	$page_id=5;
	include('includes/header.php');
    $rowsPerPage = ROWS_PER_PAGE_FRONT_END;
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg" />
    </div>
	<?php 
        include('includes/menu.php'); 
    ?>
    <div class="container">
    	<div class="inner_page_mainheading">
            <h1>Packages</h1>
            <hr>
        </div>
        <div class="row packing_inner">
            <?php
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $pSelect = "SELECT * FROM `".TABLE_PACKAGE."`";
            $pSelect = "SELECT p.ID, p.package_name, p.night_no, p.day_no, p.hotel_id, p.description, p.rate, h.place FROM `".TABLE_PACKAGE."` p INNER JOIN `".TABLE_HOTEL."` h ON p.hotel_id = h.ID";
            $pRes = $db->query($pSelect);
            $number = mysql_num_rows($pRes);
            $pageNum = 1;
            if ($number == 0) {
                ?>
                <div class="col-lg-12"><h5 class="text-center">There is no packages currently.</h5></div>
                <?php
            } else {

                if (isset($_GET['page'])) {
                    $pageNum = $_GET['page'];
                } else {
                    $pageNum = 1;
                }
                $offset = ($pageNum - 1) * $rowsPerPage;
                $pRes2 = $db->query($pSelect." LIMIT $offset, $rowsPerPage");
                $i = $offset + 1;
                while ($pRow = mysql_fetch_array($pRes2)) {
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 packing_item">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0">
                                <?php
                                $pImRes = $db->query("SELECT image_url FROM `".TABLE_PACKAGE_IMAGE."` WHERE package_id = ".$pRow['ID']." ORDER BY ID DESC LIMIT 1");
                                if (mysql_num_rows($pImRes) > 0) {
                                    $pImRow = mysql_fetch_array($pImRes);
                                    ?>
                                    <img src="<?= $pImRow['image_url']; ?>" alt="<?= $pRow['package_name'] ?>"; />
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:10px">
                                <h4><?= $pRow['package_name'] ?></h4>
                                <p><?= $pRow['place'] ?><br/>
                                </p>
                                <div class="rate"> <div class="left_sectn">Rate : <span>&#x20B9; <?= $pRow['rate'] ?>/-</span><span class="period"> per room / night</span></div> <a href="package-view.php?package=<?= $pRow['ID']; ?>" class="btn btn-primary" style="height:24px;line-height:11px;font-size:12px;">Konw more!</a> </div>

                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="pager_wrap">
            <div style="text-align: center";>
                <div class="pagerSC" align="center" style="display: inline-block;">
                    <?php

                    $query   =  $db->query($pSelect);
                    $numrows = mysql_num_rows($query);
                    $maxPage = ceil($numrows/$rowsPerPage);
                    $self = $_SERVER['PHP_SELF'];
                    $nav  = '';
                    if ($pageNum - 5 < 1) {
                        $pagemin = 1;
                    } else {
                        $pagemin = $pageNum - 5;
                    };
                    if ($pageNum + 5 > $maxPage) {
                        $pagemax = $maxPage;
                    } else {
                        $pagemax = $pageNum + 5;
                    };

                    $search=@$_REQUEST['search'];
                    for($page = $pagemin; $page <= $pagemax; $page++)
                    {
                        if ($page == $pageNum)
                        {
                            $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
                        }
                        else
                        {
                            if(@$search)
                            {
                                $nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
                            }
                            else
                            {
                                $nav .= " <a href=\"$self?page=$page\">$page</a> ";
                            }
                        }
                    }
                    ?>
                    <?php
                    if ($pageNum > 1)
                    {
                        $page  = $pageNum - 1;
                        if(@$search)
                        {
                            $prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
                            $first = " <a href=\"$self?page=1&search=$search\">First</a> ";
                        }
                        else
                        {
                            $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
                            $first = " <a href=\"$self?page=1\">First</a> ";
                        }
                    }
                    else
                    {
                        $prev  = '&nbsp;';
                        $first = '&nbsp;';
                    }

                    if ($pageNum < $maxPage)
                    {
                        $page = $pageNum + 1;
                        if(@$search)
                        {
                            $next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
                            $last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
                        }
                        else
                        {
                            $next = " <a href=\"$self?page=$page\">Next</a> ";
                            $last = " <a href=\"$self?page=$maxPage\">Last</a> ";
                        }
                    }
                    else
                    {
                        $next = '&nbsp;';
                        $last = '&nbsp;';
                    }
                    echo $first . $prev . $nav . $next . $last;
                    $db->close();
                    ?>
                    <div style="clear: left;"></div>
                </div>
            </div>
        </div>
    
</div>

<?php 
	include('includes/footer.php'); 
?>