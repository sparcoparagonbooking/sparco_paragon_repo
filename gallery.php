<?php 
	$page_id=6;
	include('includes/header.php'); 
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg" />
    </div>
	<?php 
        include('includes/menu.php'); 
    ?>
	<div class="container">
    	<div class="inner_page_mainheading">
            <h1>Our Gallery</h1>
            <hr>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gallery_inner">
            <div id="vlightbox1">
                <?php
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db->connect();
                $gRes = $db->query("SELECT * FROM `".TABLE_GALLERY."`");
                if (mysql_num_rows($gRes) > 0) {
                    while ($gRow = mysql_fetch_array($gRes)) {
                        ?>
                        <a class="vlightbox1" href="<?= $gRow['image_url'] ?>" title="<?= $gRow['description'] ?>"><img src="<?= $gRow['image_url'] ?>" alt=""/></a>
                        <?php
                    }
                }
                $db->close();
                ?>
            </div>
         </div>
        </div>
    </div> 
    
<?php 
	include('includes/footer.php'); 
?>