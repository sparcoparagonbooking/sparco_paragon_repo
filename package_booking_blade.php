<?php
require("config/config.inc.php");
require("config/Database.class.php");
require("config/Application.class.php");
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_REQUEST["package"] || !$_REQUEST["checkInDate"] || !$_REQUEST["noOfRooms"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill valid details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $data["check_in_date"] = $App->dbformat_date($App->convert($_REQUEST["checkInDate"]));
            $data["check_in_time"] = "9:00 am";
            $data["check_out_date"] = $App->dbformat_date($App->convert($_REQUEST["checkInDate"]));
            $data["check_out_time"] = "5:00 am";
            $data["adult_no"] = $App->convert($_REQUEST["noOfAdults"]);
            $data["children_no"] = $App->convert($_REQUEST["noOfChildren"]);
            $data["room_no"] = $App->convert($_REQUEST["noOfRooms"]);
            $data["rate"] = $App->convert($_REQUEST["rate"]);
            $data["booking_type"] = "online";
            $data["user_id"] = 0;
            $data["customer_id"] = $_SESSION['customer_id'];
            $data['package_id'] = $App->convert($_REQUEST["package"]);
            $success = $db->query_insert(TABLE_PACKAGE_BOOKING, $data);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "You have booked successfully");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        //echo $_SESSION['msg'];
        header("location:package.php");
        break;
    case "edit" :
        if (!$_REQUEST["name"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $eid = @mysql_escape_string($_REQUEST['eid']);
            if ($eid == 1) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Sorry. You can't edit the details of super admin!");
            } else {
                $success = 0;
                $data["name"] = $App->convert($_REQUEST["name"]);
                $data["address"] = $App->convert($_REQUEST["address"]);
                $data["phone"] = $App->convert($_REQUEST["phone"]);
                $data["email"] = $App->convert($_REQUEST["email"]);
                $data['hotel_id'] = $App->convert($_REQUEST['hotel']);
                $success = $db->query_update(TABLE_USER, $data, "ID = '".$eid."'");
                if ($success) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("success", "User details updated successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                }
            }
            $db->close();
        }
        header("location:edit.php?eid=".$eid);
        break;
    case "delete" :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = @mysql_escape_string($_REQUEST['eid']);
            if ($eid == 1) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Sorry. You can't delete super admin!");
            } else {
                $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db->connect();
                $success = 0;
                $success = @mysql_query("DELETE FROM `" . TABLE_LOGIN . "` WHERE user_id = '{$eid}'");
                if ($success) {
                    $uSuccess = @mysql_query("DELETE FROM `" . TABLE_USER . "` WHERE ID = '{$eid}'");
                }
                if ($success && $uSuccess) {
                    $_SESSION['msg'] = $App ->sessionMsgCreate("success", "user deleted successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong! Please try again.");
                }
                $db->close();
            }
        }
        header("location:index.php");
        break;
}