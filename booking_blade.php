<?php
require("config/config.inc.php");
require("config/Database.class.php");
require("config/Application.class.php");
//session_start();
if (!isset($_SESSION['booking'])) {
    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill valid details!");
} else {
    $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    $db->connect();
    $success = 0;
    $booking    =   $_SESSION['booking'];
    $numOfRooms =   $booking['numOfRooms'];
    $data["check_in_date"] = $App->dbformat_date($App->convert($booking["checkInDate"]));
    $data["check_in_time"] = "9:00 am";
    $data["check_out_date"] = $App->dbformat_date($App->convert($booking["checkOutDate"]));
    $data["check_out_time"] = "6:00 pm";
    $data["adult_no"] = $App->convert($booking["adultNo"]);
    $data["children_no"] = $App->convert($booking["kidNo"]);
    $data["booking_type"] = "online";
    $data["user_id"] = 0;
    $data["customer_id"] = $_SESSION['customer_id'];

    $roomDetails = unserialize($booking['rooms']);

    $successArray = array();
    //var_dump($roomDetails);die;
    $i = 0;
    foreach ($roomDetails as $room){
        $i++;
        if($i<=$numOfRooms){ // number of rooms will be there in the selected hotel but we need to save only that mach rooms that we need
            $data["room_no"] = $App->convert($room["room_no"]);
            $data["hotel_id"] = $App->convert($room["hotelId"]);
            $data["room_id"] = $App->convert($room["roomId"]);
            $data["rate"] = $App->convert($room["rate_per_day"]);

            $success = $db->query_insert(TABLE_BOOKING, $data);
            array_push($successArray,$success);
        }else{
            break;
        }
    }
    if ($success) {
        $data1['booking_ids']    =   implode(',',$successArray);
        $data1['customer_id']   =   $_SESSION['customer_id'];
        $data1['hotel_id']      =   $roomDetails[0]['hotelId'];
        $data1['no_of_rooms']   =   $booking['numOfRooms'];
        $data1['paid_amount']   =   $booking['numOfRooms']*$roomDetails[0]['rate_per_day'];
        $data1['received_on']   =   date("Y-m-d");

        $db->query_insert(TABLE_PAYMENTS, $data1);

        $_SESSION['msg'] = $App->sessionMsgCreate("success", "You have booked successfully");
    } else {
        $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
    }
    unset($_SESSION['booking']);
    $db->close();
}
$_SESSION['msg'] = $App->sessionMsgCreate("success", "Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail");

header("location:index.php");
