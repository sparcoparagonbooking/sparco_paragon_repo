<?php
$page_id = 5.1;
include('includes/header.php');
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg"/>
    </div>
<?php
include('includes/menu.php');
?>

<?php
if (@!$_REQUEST['package']) {
    header('Location: package.php');
} else {
    $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    $db->connect();
    $packageId = @mysql_escape_string($_REQUEST['package']);
    $pRes = $db->query("SELECT p.ID, p.package_name, p.night_no, p.day_no, p.description, p.airport_transportation, p.laundry_service, p.internet_wifi, p.break_fast, p.lunch, p.dinner, p.candle_light_dinner, p.other_inclusion, p.exclusion, p.rate, h.hotel_name, h.place FROM `" . TABLE_PACKAGE . "` p INNER JOIN `" . TABLE_HOTEL . "` h ON p.hotel_id = h.ID WHERE p.ID = '" . $packageId . "'");
    if (mysql_num_rows($pRes) > 0) {
        $pRow = mysql_fetch_array($pRes);
        $features = array();
        if ($pRow['airport_transportation']) {
            array_push($features, 'Airport Transportation');
        }
        if ($pRow['laundry_service']) {
            array_push($features, 'Laundry Service');
        }
        if ($pRow['internet_wifi']) {
            array_push($features, 'Internet/WiFi');
        }
        if ($pRow['break_fast']) {
            array_push($features, 'Break Fast');
        }
        if ($pRow['lunch']) {
            array_push($features, 'Lunch');
        }
        if ($pRow['dinner']) {
            array_push($features, 'Dinner');
        }
        if ($pRow['candle_light_dinner']) {
            array_push($features, 'Candle Light Dinner');
        }
        ?>
        <div class="container">
            <div class="inner_page_mainheading">
                <h2><?= $pRow['package_name']; ?></h2>
            </div>
            <div class="row package_view_inner">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <?php
                    $pImgRes = $db->query("SELECT image_url FROM `" . TABLE_PACKAGE_IMAGE . "` WHERE package_id = " . $pRow['ID'] . " ORDER BY ID DESC LIMIT 5");
                    if (mysql_num_rows($pImgRes) > 0) {
                        $imgArray = array();
                        while ($pImgRow = mysql_fetch_array($pImgRes)) {
                            array_push($imgArray, $pImgRow['image_url']);
                        }
                        ?>
                        <div class="pack_view_slid">
                            <div id="amazingslider-wrapper-5"
                                 style="display:block;position:relative;max-width:900px;margin:0px auto 108px;">
                                <div id="amazingslider-5" style="display:block;position:relative;margin:0 auto;">
                                    <ul class="amazingslider-slides" style="display:none;">
                                        <?php
                                        foreach ($imgArray as $img) {
                                            ?>
                                            <li><img src="<?= $img; ?>" alt="<?= $pRow['package_name']; ?>"/></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="amazingslider-thumbnails" style="display:none;">
                                        <?php
                                        foreach ($imgArray as $img) {
                                            ?>
                                            <li><img src="<?= $img; ?>" alt="<?= $pRow['package_name']; ?>"/></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="pack_view_content">
                        <div class="clearfix">
                            <p class="hotel_name"><?= $pRow['hotel_name']; ?></p>
                            <p class="place">Place : <?= $pRow['place']; ?></p>
                        </div>
                        <p class="description"><?= $pRow['description']; ?></p>
                        <div class="rate">
                            <div class="left_sectn">Rate : <span>&#x20B9; <?= $pRow['rate']; ?>/-</span>
                            </div>
                            <p class="days">Days<span><?= $pRow['day_no']; ?></span></p>
                            <p class="nights">Nights<span><?= $pRow['night_no']; ?></span></p>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="features">
                                    <h4>Special Features</h4>
                                    <?php
                                    $k = 1;
                                    foreach ($features as $feature) {
                                        ?>
                                        <span><i class="fa fa-arrow-circle-right"></i> <?= $feature; ?></span>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="book_now">
                                    <a href="package-payment.php?package=<?= $packageId; ?>">Book Now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <?php
    }
    $db->close();
}
?>
<?php
include('includes/footer.php');
?>