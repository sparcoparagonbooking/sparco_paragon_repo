<?php
$page_id = 5.2;
include('includes/header.php');
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg"/>
    </div>
<?php
include('includes/menu.php');
if (!isset($_SESSION['customer_id']) || !$_SESSION['customer_id'] || $_SESSION['customer_id'] == "" || !isset($_SESSION['user_type']) || !$_SESSION['user_type'] || $_SESSION['user_type'] != "customer") {
    header('Location: login.php');
}
if (!isset($_REQUEST['package']) || !$_REQUEST['package'] || $_REQUEST['package'] == "") {
    header('Location: package.php');
} else {
    $packageId = @mysql_escape_string($_REQUEST['package']);
}
?>
    <div class="container">
        <div class="inner_page_mainheading">
            <h1>Package Booking</h1>
            <hr>

        </div>
        <div class="row booking_inner">
            <?php
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $pRes = $db->query("SELECT p.ID as pID, p.package_name, p.rate, h.ID as hotel, h.hotel_name, h.place FROM `".TABLE_PACKAGE."` p INNER JOIN `".TABLE_HOTEL."` h ON p.hotel_id = h.ID WHERE p.ID = '".$packageId."'");
            if (mysql_num_rows($pRes) > 0) {
                $pRow = mysql_fetch_array($pRes);
                ?>
                <form id="msform" class="package_booking" method="POST" action="package_booking_blade.php?op=index">
                    <input type="hidden" name="rate" value="<?= $pRow['rate']; ?>">
                    <input type="hidden" name="package" value="<?= $packageId; ?>">
                   
                    <fieldset>
                        <h2 class="fs-title">Confirm Booking</h2>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 booking_search_item">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding:0">
                                    <?php
                                    $pImRes = $db->query("SELECT * FROM `".TABLE_PACKAGE_IMAGE."` WHERE package_id = '".$packageId."' ORDER BY ID DESC LIMIT 1");
                                    if (mysql_num_rows($pImRes) > 0) {
                                        $pImRow = mysql_fetch_array($pImRes);
                                        ?>
                                        <img src="<?= $pImRow['image_url']; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-top:10px">
                                    <h4><?= $pRow['package_name'] ?></h4>
                                    <div class="form-group">
                                        Hotel: <?= $pRow['hotel_name'].' '.$pRow['place']; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Check In Date</label>
                                        <input name="checkInDate" type="text" class="ui_calander" value="<?= date('d-m-Y');  ?>">
                                    </div>
                                    <!--<div class="form-group">
                                        <label>Check Out Date</label>
                                        <input name="checkOutDate" type="text" class="ui_calander">
                                    </div>-->
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>No of Adults</label>
                                                <input type="number" min="1" value="1" name="noOfAdults">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label>No of Children</label>
                                                <input type="number" min="0" value="0" name="noOfChildren">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>No. of Rooms</label>
                                        <input type="number" min="1" value="1" name="noOfRooms">
                                    </div>
                                    <div class="rate">
                                        <div class="left_sectn" data-rate="<?= $pRow['rate']; ?>">Rate : <span>&#x20B9; <?= $pRow['rate']; ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-danger">Confirm</button>
                    </fieldset>
                </form>
                <?php
            }
            $db->close();
            ?>
        </div>
    </div>

<?php
include('includes/footer.php');
?>