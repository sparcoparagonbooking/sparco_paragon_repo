<?php
$page_id = 1;
include('includes/header.php');
?>
    <div class="banner">
        <div class="slider">
            <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:100%;margin:0 auto;">
                <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                    <ul class="amazingslider-slides" style="display:none;">
                        <li><img src="images/slid_2.jpg" alt="slid_1" title="slid_1"/>
                        </li>
                        <!--<li><img src="images/slid_1.jpg" alt="slid_1" title="slid_1"/>
                        </li>
                        <li><img src="images/slid_1.jpg" alt="slid_1" title="slid_1"/>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="pragon_red">
            <img src="images/paragon_red.jpg" alt="red_para">
        </div>
        <div class="container">
            <div class="check_fourm">
                <div class="chek_top">
                    <span><i class="fa fa-check"></i></span>
                    <div class="check_head"> CHECK <br class="hidden-xs hidden-sm hidden-md"/>
                        <b>AVAILABILITY</b>
                    </div>
                </div>

                <form method="POST" action="booking-search.php">
                    <select class="room_typ" name="place" required>
                        <option value="">Select</option>
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db->connect();
                        $placeRes = $db->query("SELECT place FROM `".TABLE_HOTEL."`");
                        if (mysql_num_rows($placeRes) > 0) {
                            while ($placeRow = mysql_fetch_array($placeRes)) {
                                ?>
                                <option value="<?= $placeRow['place']; ?>"><?= $placeRow['place']; ?></option>
                                <?php
                            }
                        }
                        $db->close();
                        ?>
                    </select>
                    <!-- <input type="date" placeholder="Check in Date">-->
                    <input class="form-control" id="demo" type="text" name="checkInDate" placeholder="Check in Date" readonly
                           style="padding-right: 160px;" required>
                    <input class="form-control" id="demo1" type="text" name="checkOutDate" placeholder="Check Out Date" readonly
                           style="padding-right: 160px;" required>
                    <input class="form-control num_pers" style="margin-right: 5px;" id="adult_no" type="number" name="adult_no" placeholder="Adult"
                           min="1">
                    <input class="form-control num_pers" id="kids_no" type="number" name="kids_no" placeholder="Children" min="0">
                    <button>Search</button>
                </form>
            </div>
        </div>
    </div>
<?php
include('includes/menu.php');
?>
    <div class="why">
        <div class="container">
            <h1>Why choose us ?</h1>
            <hr>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="why_img_box">
                                <a href="#"><img src="images/Function-Rooms-Pyrmont.jpg" alt="fn">
                                    <span>Conference Hall</span></a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="why_img_box">
                                <a href="#"><img src="images/swimming_pool.jpg" alt="fn">
                                    <span>Swimming Pool</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7">
                            <div class="why_img_box">
                                <a href="#"><img src="images/business-meetings.jpg" alt="fn">
                                    <span>Tea Spot</span></a>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="why_img_box">
                                        <a href="#"><img src="images/gymnasium.jpg" alt="fn">
                                            <span>Gymnasium</span></a>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="why_img_box">
                                        <a href="#"><img src="images/restaurent.jpg" alt="fn">
                                            <span>Restaurant</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="meet_our">
        <div class="container">
            <h1>Meet Our Hotels</h1>
            <hr>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="">
                        <?php
                        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db->connect();
                        $hRes = $db->query("SELECT * FROM `".TABLE_HOTEL."`");
                        if (mysql_num_rows($hRes) > 0) {
                            ?>
                            <div id="amazingcarousel-container-4">
                                <div id="amazingcarousel-4"
                                     style="display:none;position:relative;width:100%;max-width:1200px;margin:0px auto 0px;">
                                    <div class="amazingcarousel-list-container">
                                        <ul class="amazingcarousel-list">
                                            <?php
                                            while($hRow = mysql_fetch_array($hRes)) {
                                                ?>
                                                <li class="amazingcarousel-item">
                                                    <div class="amazingcarousel-item-container">
                                                        <div class="amazingcarousel-text">
                                                            <div class="meet_hotel_slid">
                                                                <h2><?= $hRow['hotel_name']; ?></h2>
                                                                <p><?= $hRow['description']; ?></p>
                                                                <?php
                                                                if ($hRow['place']) {
                                                                    ?>
                                                                    <span><i class="fa fa-map-marker"></i><?= $hRow['place']; ?></span>
                                                                    <?php
                                                                }
                                                                if ($hRow['contact_no']) {
                                                                    ?>
                                                                    <span><i class="fa fa-phone"></i><?= $hRow['contact_no']; ?></span>
                                                                    <?php
                                                                }
                                                                if ($hRow['email']) {
                                                                    ?>
                                                                    <span><i class="fa fa-envelope"></i><?= $hRow['email']; ?></span>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="amazingcarousel-image"><img src="<?= $hRow['image_url']; ?>"/></div>
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            <!--<li class="amazingcarousel-item">
                                                <div class="amazingcarousel-item-container">
                                                    <div class="amazingcarousel-text">
                                                        <div class="meet_hotel_slid">
                                                            <h2>PARAGON BANGALORE</h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                                do eiusmod tempor. dolor sit amet, consectetur adipiscing
                                                                elit, Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                elit, sed do eiusmod tempor. dolor sit amet, consectetur
                                                                adipiscing elit,</p>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <a href="#">Explore now!</a>
                                                        </div>
                                                    </div>
                                                    <div class="amazingcarousel-image"><img src="images/hotel.png"/></div>
                                                    <div style="clear:both;"></div>
                                                </div>
                                            </li>
                                            <li class="amazingcarousel-item">
                                                <div class="amazingcarousel-item-container">
                                                    <div class="amazingcarousel-text">
                                                        <div class="meet_hotel_slid">
                                                            <h2>PARAGON BANGALORE</h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                                do eiusmod tempor. dolor sit amet, consectetur adipiscing
                                                                elit, Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                elit, sed do eiusmod tempor. dolor sit amet, consectetur
                                                                adipiscing elit,</p>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <a href="#">Explore now!</a>
                                                        </div>
                                                    </div>
                                                    <div class="amazingcarousel-image"><img src="images/hotel.png"/></div>
                                                    <div style="clear:both;"></div>
                                                </div>
                                            </li>
                                            <li class="amazingcarousel-item">
                                                <div class="amazingcarousel-item-container">
                                                    <div class="amazingcarousel-text">
                                                        <div class="meet_hotel_slid">
                                                            <h2>PARAGON BANGALORE</h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                                do eiusmod tempor. dolor sit amet, consectetur adipiscing
                                                                elit, Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                elit, sed do eiusmod tempor. dolor sit amet, consectetur
                                                                adipiscing elit,</p>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <span><i class="fa fa-check-square-o"></i> lorem quis bibendum auctor.</span>
                                                            <a href="#">Explore now!</a>
                                                        </div>
                                                    </div>
                                                    <div class="amazingcarousel-image"><img src="images/hotel.png"/></div>
                                                    <div style="clear:both;"></div>
                                                </div>
                                            </li>-->
                                        </ul>
                                        <div class="amazingcarousel-prev"></div>
                                        <div class="amazingcarousel-next"></div>
                                    </div>
                                    <div class="amazingcarousel-nav"></div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="our_special">
        <div class="container">
            <h1>OUR SPECIAL ROOMS</h1>
            <hr>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div id="amazingslider-wrapper-2"
                         style="display:block;position:relative;max-width:100%;padding-left:0px; padding-right:290px;margin:0px auto 0px;">
                        <div id="amazingslider-2" style="display:block;position:relative;margin:0 auto;">
                            <ul class="amazingslider-slides" style="display:none;">
                                <li><img src="images/twin_hilton_guestroom_plus.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Twin Hilton Guestroom Plus"
                                         data-description="2 Twin Beds, WiFi and wired Internet access, flat-screen TV, DVD player, Coffee/tea maker, minibar, and 24-hour room service, Hypo-allergenic bedding and blackout drapes/curtains, Air conditioning and daily housekeeping."/>
                                    <!--<a href="#">
                                        <button class="as-btn-blueborder-medium">Book Now!</button>
                                    </a>-->
                                </li>
                                <li><img src="images/twin_hilton_delux.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Twin Hilton Deluxe Room"
                                         data-description="2 Twin Beds, 355 sq feet (33 sq meters), WiFi and wired Internet access, flat-screen TV, DVD player, Coffee/tea maker, minibar, and 24-hour room service, Hypo-allergenic bedding and blackout drapes/curtains, Air conditioning and daily housekeeping."/>
                                    <!--<a href="#">
                                        <button class="as-btn-blueborder-medium">Book Now!</button>
                                    </a>-->
                                </li>
                                <li><img src="images/king_hilton_superior_room.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="King Hilton Superior Room"
                                         data-description="1 King Bed, 377 sq feet (35 sq meters), WiFi and wired Internet access, flat-screen TV, DVD player, Private bathroom, deep soaking bathtub, Phone, laptop-compatible safe, and iron/ironing board; rollaway/extra beds and free cribs/infant beds available on request"/>
                                    <!--<a href="#">
                                        <button class="as-btn-blueborder-medium">Book Now!</button>
                                    </a>-->
                                </li>
                                <li><img src="images/double_hilton_guestroom_plus.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Double Hilton Guestroom Plus"
                                         data-description="1 Full Bed, 355 sq feet (33 sq meters), WiFi and wired Internet access, flat-screen TV, DVD player, Coffee/tea maker, minibar, and 24-hour room service, Laptop-compatible safe, iron/ironing board, and desk; rollaway/extra beds and free cribs/infant beds available on request"/>
                                    <!--<a href="#">
                                        <button class="as-btn-blueborder-medium">Book Now!</button>
                                    </a>-->
                                </li>
                            </ul>
                            <ul class="amazingslider-thumbnails" style="display:none;">
                                <li><img src="images/twin_hilton_guestroom_plus.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Cosy &lt;br&gt; Double Room"/></li>
                                <li><img src="images/twin_hilton_delux.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Cosy &lt;br&gt; Double Room"/></li>
                                <li><img src="images/king_hilton_superior_room.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Cosy &lt;br&gt; Double Room"/></li>
                                <li><img src="images/double_hilton_guestroom_plus.jpg" alt="Cosy &lt;br&gt; Double Room"
                                         title="Cosy &lt;br&gt; Double Room"/></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="resturant_part">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="resturant_slid">
                    <h4>Restaurant</h4>
                    <div id="amazingslider-wrapper-3"
                         style="display:block;position:relative;max-width:100%;margin:0 auto;">
                        <div id="amazingslider-3" style="display:block;position:relative;margin:0 auto;">
                            <ul class="amazingslider-slides" style="display:none;">
                                <li><img src="images/restaurant_1.jpg" alt="j1" title="Paragon Restaurant"/>
                                </li>
                                <li><img src="images/restaurant_2.jpg" alt="j1" title="Paragon Restaurant"/>
                                </li>
                                <li><img src="images/restaurant_3.jpg" alt="j1" title="Paragon Restaurant"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="special_menu">
                    <h4>SPECIAL MENU</h4>
                    <div class="row">
                        <?php
                        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db->connect();
                        $foodRes = $db->query("SELECT food_name, image_url FROM `" . TABLE_FOOD . "` ORDER BY ID DESC LIMIT 4");
                        if (mysql_num_rows($foodRes) > 0) {
                        $i = 0;
                        while ($foodRow = mysql_fetch_array($foodRes)) {
                        ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="our_special_box">
                                <a href="javascript:void(0)">
                                    <img src="<?= $foodRow['image_url']; ?>" alt="<?= $foodRow['food_name']; ?>">
                                    <div class="overlay">
                                        <h5><?= $foodRow['food_name']; ?></h5>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php
                        $i += 1;
                        if ($i == 2) {
                        $i = 0;
                        ?>
                    </div>
                    <div class="row">
                        <?php

                        }
                        }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include('includes/footer.php');
?>