<?php 
	$page_id=2;
	include('includes/header.php'); 
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg" />
    </div>
	<?php 
        include('includes/menu.php'); 
    ?>
	<div class="container">
    	<div class="inner_page_mainheading">
            <h1>About Us</h1>
            <hr>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_main">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left:0">
            	<img src="images/hotel.jpg">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <p>Paragon  offers comfortable accommodation in the heart of Bangalore, the shoppingcity. The Express Start Breakfast buffet and free Wi-Fi are included with every room, and signature bedding ensures a good night's sleep. The hotel has event facilities and a restaurant. Guests will find the comforts of home, the conveniences of the office, and a wide variety of amenities to cater to their needs. From fitness to business to family fun,PARAGON is there to provide an excellent experience. The regality of the interiors is a living testament to the levels of sophistication and grace, ensured to please the most discerning guests. Outstanding service  and our discreet and charming staff will not disappoint. They will provide more than a warm welcome; they will make each and every stay with us pleasurable and memorable. We look forward to welcoming you to The Bayswater Inn Hotel and we wish you a very pleasant stay with us!</p>
            </div>
        </div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 about_left">
         	<div class="col-xs-12 contact_head_sub">Why chose us ?</div>
            <ul class="wlcome_itm" style="color:#000;">
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Refined luxury of heritage</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Efficient service and amenities</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Assurance of quality</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i>The Best price</li>
            </ul>
		 </div>
         <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 about_left">
         	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission" style="padding:0">
            	
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_content" style="padding:0">
                	<h3>Our Mission</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy 
                    text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission" style="padding:0">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_content" style="padding:0">
                	<div><h3>Our Vision</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy 
                    text ever since the 1500s.</p></div>
                </div>
            </div>
		 </div>-->
        </div>
    </div> 
    
<?php 
	include('includes/footer.php'); 
?>