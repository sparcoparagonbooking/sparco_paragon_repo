<?php
$page_id = 3.2;
include('includes/header.php');
if (@$_SESSION['customer_id'] != "") {
    header('Location: '.$_SERVER["HTTP_REFERER"]);
}
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg"/>
    </div>
<?php
include('includes/menu.php');
?>
    <div class="container">
        <div class="inner_page_mainheading">
            <h1>Login</h1>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h3>Login</h3>
                        <form action="login_blade.php" method="POST" class="default_form">
                            <div class="form-group">
                                <label>User Name <span class="valid">*</span></label>
                                <input type="text" name="userName" placeholder="User name" required>
                            </div>
                            <div class="form-group">
                                <label>Password <span class="valid">*</span></label>
                                <input type="password" name="password" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Login">
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h3>Register</h3>
                        <form action="reg_blade.php?op=index" method="POST" class="default_form">
                            <div class="form-group">
                                <label>Name <span class="valid">*</span></label>
                                <input type="text" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <textarea name="address" placeholder="Address"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Pin</label>
                                <input type="text" name="pin" placeholder="Pin">
                            </div>
                            <div class="form-group">
                                <label>Contact No <span class="valid">*</span></label>
                                <input type="text" name="contactNo" placeholder="Contact No" required>
                            </div>
                            <div class="form-group">
                                <label>Alternative Contact No</label>
                                <input type="text" name="altContactNo" placeholder="Alternative Contact No">
                            </div>
                            <div class="form-group">
                                <label>Email <span class="valid">*</span></label>
                                <input type="email" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label>User Name <span class="valid">*</span></label>
                                <input type="text" name="userName" placeholder="User name" required>
                            </div>
                            <div class="form-group">
                                <label>Password <span class="valid">*</span></label>
                                <input type="password" name="password" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password <span class="valid">*</span></label>
                                <input type="password" name="cPassword" placeholder="Confirm Password" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Register">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
include('includes/footer.php');
?>