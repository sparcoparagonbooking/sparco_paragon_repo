-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 28, 2017 at 11:24 PM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sparco`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE IF NOT EXISTS `tbl_booking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_date` date NOT NULL,
  `check_in_time` varchar(50) NOT NULL,
  `check_out_date` date NOT NULL,
  `check_out_time` varchar(50) NOT NULL,
  `adult_no` int(11) NOT NULL,
  `children_no` int(11) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `rate` float NOT NULL DEFAULT '0',
  `booking_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cleaning_status`
--

CREATE TABLE IF NOT EXISTS `tbl_cleaning_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cleaning_status` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_cleaning_status`
--

INSERT INTO `tbl_cleaning_status` (`ID`, `cleaning_status`) VALUES
(1, 'OK');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `alt_contact_no` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `remark` text,
  `photo` text,
  `document_img` text,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`ID`, `name`, `address`, `pin`, `contact_no`, `alt_contact_no`, `email`, `remark`, `photo`, `document_img`) VALUES
(1, 'Archana', 'archana \r\ncalicut', '', '65455456', '', 'svmcool99@gmail.com', NULL, NULL, NULL),
(2, 'Sujina', '', '', '9636363636', '', 'sujina@gmail.com', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer_details`
--

CREATE TABLE IF NOT EXISTS `tbl_customer_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `age` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `contact_no` varchar(100) NOT NULL,
  `booking_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_food`
--

CREATE TABLE IF NOT EXISTS `tbl_food` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `food_name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_food`
--

INSERT INTO `tbl_food` (`ID`, `food_name`, `description`, `image_url`) VALUES
(7, 'Food 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper lacus in massa fermentum, in pulvinar justo porta. Aliquam erat volutpat. Etiam aliquam congue libero, sed porttitor arcu convallis sit amet. Suspendisse at interdum tellus, ut rhoncus lectus. Sed eget pulvinar risus, vitae aliquam orci.', 'images/food/20160923165132.jpg'),
(8, 'Food 2', 'Nulla a arcu a augue euismod porttitor et vitae lacus. Donec aliquam lectus felis, vel viverra massa porta placerat. Phasellus sit amet ultricies nunc. Aenean vel neque sed enim varius maximus eu quis purus. Fusce fringilla neque quis nisi lobortis pharetra.', 'images/food/20160923165200.jpg'),
(9, 'Food 3', 'Cras laoreet diam massa, ac tristique elit hendrerit nec. Sed semper justo et lacus sollicitudin, a suscipit odio posuere. Proin elementum fermentum accumsan. Nunc sed tellus ut justo convallis ornare. Pellentesque in maximus nibh. Etiam porttitor, sem at vestibulum vestibulum, sem nulla interdum velit, ac tempus justo magna et mauris.', 'images/food/20160923165232.jpg'),
(10, 'Food 4', 'Nam ullamcorper metus ut porttitor elementum. Quisque mattis lectus in tempus tempor. Sed eget laoreet lorem. Etiam varius metus nisi, nec consectetur nunc sagittis sit amet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas vulputate diam magna, in egestas erat condimentum non.', 'images/food/20160923165254.jpg'),
(11, 'Food 5', 'Integer viverra arcu in sem euismod eleifend. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras non interdum urna, ut commodo augue. Donec metus felis, laoreet in nunc non, rutrum molestie ipsum. Fusce vulputate feugiat ante quis ullamcorper. Nunc sollicitudin euismod nisi, sed semper ex scelerisque sed.', 'images/food/20160923165332.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`ID`, `image_url`, `description`) VALUES
(14, 'images/gallery/20160922123854.jpg', 'Hotel1'),
(15, 'images/gallery/20160922123905.jpg', 'Hotel2'),
(16, 'images/gallery/20160922123916.jpg', 'Hotel3'),
(17, 'images/gallery/20160922123950.jpg', 'Hotel 4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hotel`
--

CREATE TABLE IF NOT EXISTS `tbl_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_hotel`
--

INSERT INTO `tbl_hotel` (`ID`, `hotel_name`, `place`, `address`, `contact_no`, `email`, `description`, `image_url`) VALUES
(2, 'New Paragon Residency', 'Yeshwanthpur, Bengaluru', '1 st Cross Road 5-B/1\r\nYeshwanthpur, Bengaluru', '08023571341', 'info@paragonbooking.com', '', 'images/hotel/20170429145949.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE IF NOT EXISTS `tbl_login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `usertype` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`ID`, `username`, `password`, `usertype`, `user_id`) VALUES
(1, 'admin', 'cb989ba59f6e9ba11d9d73b20dfe59f1', 'admin', 0),
(2, 'staff', '6e07f048f2e7dcc96dc484c544df53df', 'staff', 2),
(16, 'archana', '46ff90e8e3c3c1cb6cae3a539082ffc9', 'customer', 1),
(17, 'sujina', '2e7d489567c2df6ac3ac7b03821063df', 'customer', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

CREATE TABLE IF NOT EXISTS `tbl_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(500) NOT NULL,
  `night_no` int(11) NOT NULL,
  `day_no` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `airport_transportation` tinyint(1) NOT NULL,
  `laundry_service` tinyint(1) NOT NULL,
  `internet_wifi` tinyint(1) NOT NULL,
  `break_fast` tinyint(1) NOT NULL,
  `lunch` tinyint(1) NOT NULL,
  `dinner` tinyint(1) NOT NULL,
  `candle_light_dinner` tinyint(1) NOT NULL,
  `other_inclusion` text NOT NULL,
  `exclusion` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` float DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_booking`
--

CREATE TABLE IF NOT EXISTS `tbl_package_booking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_date` date NOT NULL,
  `check_in_time` varchar(50) NOT NULL,
  `check_out_date` date NOT NULL,
  `check_out_time` varchar(50) NOT NULL,
  `adult_no` int(11) NOT NULL,
  `children_no` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `rate` float NOT NULL,
  `booking_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_image`
--

CREATE TABLE IF NOT EXISTS `tbl_package_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE IF NOT EXISTS `tbl_payments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `booking_ids` text NOT NULL,
  `customer_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `paid_amount` float NOT NULL,
  `received_on` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room`
--

CREATE TABLE IF NOT EXISTS `tbl_room` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `rate_per_day` float NOT NULL,
  `tax` float NOT NULL,
  `total` float NOT NULL,
  `seasonal_rate` float NOT NULL DEFAULT '0',
  `floor_no` varchar(50) NOT NULL,
  `room_status_id` int(11) NOT NULL,
  `cleaning_status_id` int(11) NOT NULL,
  `facilities` text NOT NULL,
  `image_url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_room`
--

INSERT INTO `tbl_room` (`ID`, `hotel_id`, `room_type_id`, `room_name`, `room_no`, `rate_per_day`, `tax`, `total`, `seasonal_rate`, `floor_no`, `room_status_id`, `cleaning_status_id`, `facilities`, `image_url`) VALUES
(1, 2, 1, 'Room 1', '302', 3000, 100, 3100, 0, '5', 1, 1, '', 'images/room/20170404171056.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_status`
--

CREATE TABLE IF NOT EXISTS `tbl_room_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `room_status` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_room_status`
--

INSERT INTO `tbl_room_status` (`ID`, `room_status`) VALUES
(1, 'Ready to use');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_type`
--

CREATE TABLE IF NOT EXISTS `tbl_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_room_type`
--

INSERT INTO `tbl_room_type` (`ID`, `room_type`) VALUES
(1, 'A/C');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `name`, `address`, `phone`, `email`, `hotel_id`) VALUES
(1, 'Admin', '', '', '', 0),
(2, 'Staff', '', '', '', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
