-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 23, 2017 at 04:47 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_sparco`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE `tbl_booking` (
  `ID` int(11) NOT NULL,
  `check_in_date` date NOT NULL,
  `check_in_time` varchar(50) NOT NULL,
  `check_out_date` date NOT NULL,
  `check_out_time` varchar(50) NOT NULL,
  `adult_no` int(11) NOT NULL,
  `children_no` int(11) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `rate` float NOT NULL DEFAULT '0',
  `booking_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_booking`
--

INSERT INTO `tbl_booking` (`ID`, `check_in_date`, `check_in_time`, `check_out_date`, `check_out_time`, `adult_no`, `children_no`, `room_no`, `hotel_id`, `room_id`, `rate`, `booking_type`, `user_id`, `customer_id`) VALUES
(1, '2017-05-11', '2.30', '2017-05-13', '', 3, 1, '1', 2, 1, 5000, 'office', 2, 10),
(4, '2017-05-10', '6pm', '2017-05-18', '', 2, 2, '1', 2, 1, 6500, 'office', 2, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cleaning_status`
--

CREATE TABLE `tbl_cleaning_status` (
  `ID` int(11) NOT NULL,
  `cleaning_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cleaning_status`
--

INSERT INTO `tbl_cleaning_status` (`ID`, `cleaning_status`) VALUES
(1, 'OK'),
(2, 'Ready for Cleaning');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `alt_contact_no` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `remark` text,
  `photo` text,
  `document_img` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`ID`, `name`, `address`, `pin`, `contact_no`, `alt_contact_no`, `email`, `remark`, `photo`, `document_img`) VALUES
(1, 'Sandhya', '123\r\nwestfort', '', '4347587878998932434', '', 'aswwef@gmail.com', NULL, NULL, NULL),
(2, 'arya', '234\r\ndelhi', '', '645gfgfgfs665', '', 'ererer@gmail.com', NULL, NULL, NULL),
(3, 'theertha', '456\r\nbangalore', '', 'eg5677', '', 'adfddf@gmail.com', NULL, NULL, NULL),
(4, 'neenu', '', '', '3434', '', 'nee@yahoo.com', NULL, NULL, NULL),
(5, 'Sujina', 'Eranjhippalam', '', '9636363636', '', 'sujinabodhi@gmail.com', NULL, NULL, NULL),
(6, 'Sindhuja', '', '', '0495-0565650', '', 'sindhuja@gmail.com', NULL, NULL, NULL),
(7, 'ananya', '', '', '445', '', 'anu@gmail.com', NULL, NULL, NULL),
(8, 'renu', '', '', '435', '', 'renu@yahoo.com', NULL, NULL, NULL),
(9, 'mahi', '', '', '454', '', 'mahi@yahoo.com', NULL, NULL, NULL),
(10, 'Ram mohan pk', 'hyderabad', '', '6767676767', '', 'sdsd@gmail.com', 'tgddfee', 'images/customer/20170510133425.jpg', NULL),
(15, 'sajan', 'wsws', '', '678ghjg', '', 'gsdsdfg@gmail.com', 'iuytuyuy', NULL, NULL),
(13, 'priya anand', '1233334', '9876543', '45343434464', '565454545', 'seded@gmail.com', 'grgf', NULL, NULL),
(14, 'Akhila', '', '', '9636363636', '', 'mix@gmail.com', NULL, NULL, NULL),
(16, 'ali', 'aliexpress', '', '67656', '787', 'rtrt@gmailcom', 'ghfgh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer_details`
--

CREATE TABLE `tbl_customer_details` (
  `ID` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `age` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `contact_no` varchar(100) NOT NULL,
  `booking_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer_details`
--

INSERT INTO `tbl_customer_details` (`ID`, `name`, `age`, `gender`, `contact_no`, `booking_id`) VALUES
(3, 'varun', '30', 'm', '', 2),
(4, 'asif', '32', 'm', '', 3),
(7, 'Priya A', '29', 'f', '44657745556', 4),
(8, 'anand', '32', 'm', '546576778', 4),
(12, 'Ram mohan', '45', 'm', '6767676767', 1),
(13, 'sajan', '36', 'm', '56765', 1),
(14, 'ali', '40', 'm', '887', 2),
(15, 'raghu', '45', 'm', '0909', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_food`
--

CREATE TABLE `tbl_food` (
  `ID` int(11) NOT NULL,
  `food_name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_food`
--

INSERT INTO `tbl_food` (`ID`, `food_name`, `description`, `image_url`) VALUES
(7, 'Food 1- Mini Meals', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper lacus in massa fermentum, in pulvinar justo porta. Aliquam erat volutpat. Etiam aliquam congue libero, sed porttitor arcu convallis sit amet. Suspendisse at interdum tellus, ut rhoncus lectus. Sed eget pulvinar risus, vitae aliquam orci.', 'images/food/20160923165132.jpg'),
(8, 'Food 2', 'Nulla a arcu a augue euismod porttitor et vitae lacus. Donec aliquam lectus felis, vel viverra jhjhjkmassa porta placerat. Phasellus sit amet ultricies nunc. Aenean vel neque sed enim varius maximus eu quis purus. Fusce fringilla neque quis nisi lobortis pharetra.', 'images/food/20160923165200.jpg'),
(9, 'Food 3', 'Cras laoreet diam massa, ac tristique elit gfgfghendrerit nec. Sed semper justo et lacus sollicitudin, a suscipit odio posuere. Proin elementum fermentum accumsan. Nunc sed tellus ut justo convallis ornare. Pellentesque in maximus nibh. Etiam porttitor, sem at vestibulum vestibulum, sem nulla interdum velit, ac tempus justo magna et mauris.', 'images/food/20160923165232.jpg'),
(10, 'Food 4', 'Nam ullamcorper metus ut porttitor elementum. Quisque mattis lectus in tempus tempor. Sed eget laoreet lorem. Etiam varius metus nisi, nec consectetur nunc sagittis sit amet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas vulputate diam magna, in egestas erat condimentum non.', 'images/food/20160923165254.jpg'),
(11, 'Food 5', 'Integer viverra arcu in sem euismod eleifend. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras non interdum urna, ut commodo augue. Donec metus felis, laoreet in nunc non, rutrum molestie ipsum. Fusce vulputate feugiat ante quis ullamcorper. Nunc sollicitudin euismod nisi, sed semper ex scelerisque sed.', 'images/food/20160923165332.jpg'),
(12, 'Food 6', 'Malabar Chicken dum Biriyani', 'images/food/20170510145603.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `ID` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`ID`, `image_url`, `description`) VALUES
(14, 'images/gallery/20160922123854.jpg', 'Hotel1'),
(15, 'images/gallery/20160922123905.jpg', 'Hotel2'),
(16, 'images/gallery/20160922123916.jpg', 'Hotel3'),
(17, 'images/gallery/20160922123950.jpg', 'Hotel 4'),
(18, 'images/gallery/20170510150430.jpg', 'Special Delight');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hotel`
--

CREATE TABLE `tbl_hotel` (
  `ID` int(11) NOT NULL,
  `hotel_name` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_hotel`
--

INSERT INTO `tbl_hotel` (`ID`, `hotel_name`, `place`, `address`, `contact_no`, `email`, `description`, `image_url`) VALUES
(2, 'New Paragon Residency', 'Yeshwanthpur, Bengaluru', '1 st Cross Road 5-B/1\r\nYeshwanthpur, Bengaluru', '08023571341', 'info@paragonbooking.com', 'awesome view', 'images/hotel/20170429145949.jpg'),
(5, 'New Paragon Regency', 'Bangalore', 'Near Gruda mall', '789789', 'reg@yahoo.com', '', 'images/hotel/20170509165610.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `ID` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `usertype` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`ID`, `username`, `password`, `usertype`, `user_id`) VALUES
(1, 'admin', 'cb989ba59f6e9ba11d9d73b20dfe59f1', 'admin', 0),
(2, 'staff', '6e07f048f2e7dcc96dc484c544df53df', 'staff', 2),
(16, 'sandy', 'd686a53fb86a6c31fa6faa1d9333267e', 'customer', 1),
(17, 'aryaraju', '03c017f682085142f3b60f56673e22dc', 'customer', 2),
(18, 'theertham', 'ca74c7078d814932ae8f4df447e8ea64', 'customer', 3),
(20, 'sujina', '2e7d489567c2df6ac3ac7b03821063df', 'customer', 5),
(21, 'sindhu', 'cb626e8cb0e7d26c8bcbe8a7d31f980e', 'customer', 6),
(22, 'anu', '89a4b5bd7d02ad1e342c960e6a98365c', 'customer', 7),
(23, 'renuraj', '65a1223dae83b8092c4edba0823a793c', 'customer', 8),
(24, 'mahima', '61c1c234b7c819e9e37a33f3a8b21713', 'customer', 9),
(25, 'staff3', '8f03660f569ce4023dddaea0bf560d74', 'staff', 3),
(27, 'akhila', '81dc9bdb52d04dc20036dbd8313ed055', 'customer', 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

CREATE TABLE `tbl_package` (
  `ID` int(11) NOT NULL,
  `package_name` varchar(500) NOT NULL,
  `night_no` int(11) NOT NULL,
  `day_no` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `airport_transportation` tinyint(1) NOT NULL,
  `laundry_service` tinyint(1) NOT NULL,
  `internet_wifi` tinyint(1) NOT NULL,
  `break_fast` tinyint(1) NOT NULL,
  `lunch` tinyint(1) NOT NULL,
  `dinner` tinyint(1) NOT NULL,
  `candle_light_dinner` tinyint(1) NOT NULL,
  `other_inclusion` text NOT NULL,
  `exclusion` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package`
--

INSERT INTO `tbl_package` (`ID`, `package_name`, `night_no`, `day_no`, `hotel_id`, `description`, `airport_transportation`, `laundry_service`, `internet_wifi`, `break_fast`, `lunch`, `dinner`, `candle_light_dinner`, `other_inclusion`, `exclusion`, `user_id`, `rate`) VALUES
(2, 'Vaccation Package', 2, 3, 2, '', 0, 1, 1, 1, 0, 0, 0, '', '', 2, 12500),
(3, 'Family Package', 1, 2, 2, '', 0, 0, 1, 1, 0, 0, 0, '', '', 2, 4800);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_booking`
--

CREATE TABLE `tbl_package_booking` (
  `ID` int(11) NOT NULL,
  `check_in_date` date NOT NULL,
  `check_in_time` varchar(50) NOT NULL,
  `check_out_date` date NOT NULL,
  `check_out_time` varchar(50) NOT NULL,
  `adult_no` int(11) NOT NULL,
  `children_no` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `rate` float NOT NULL,
  `booking_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package_booking`
--

INSERT INTO `tbl_package_booking` (`ID`, `check_in_date`, `check_in_time`, `check_out_date`, `check_out_time`, `adult_no`, `children_no`, `room_no`, `rate`, `booking_type`, `user_id`, `customer_id`, `package_id`) VALUES
(1, '2017-05-10', '3pm', '2017-05-12', '', 1, 1, 1, 7000, 'office', 2, 15, 3),
(2, '2017-05-19', '7pm', '2017-05-26', '', 2, 1, 1, 7000, 'office', 2, 16, 3),
(4, '2017-04-10', '9:00 am', '2017-04-10', '5:00 am', 1, 0, 1, 12500, 'online', 0, 0, 2),
(5, '2017-04-11', '9:00 am', '2017-04-11', '5:00 am', 1, 0, 1, 4800, 'online', 0, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_image`
--

CREATE TABLE `tbl_package_image` (
  `ID` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `image_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `ID` int(11) NOT NULL,
  `booking_ids` text NOT NULL,
  `customer_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `paid_amount` float NOT NULL,
  `received_on` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room`
--

CREATE TABLE `tbl_room` (
  `ID` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `rate_per_day` float NOT NULL,
  `tax` float NOT NULL,
  `total` float NOT NULL,
  `seasonal_rate` float NOT NULL DEFAULT '0',
  `floor_no` varchar(50) NOT NULL,
  `room_status_id` int(11) NOT NULL,
  `cleaning_status_id` int(11) NOT NULL,
  `facilities` text NOT NULL,
  `image_url` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room`
--

INSERT INTO `tbl_room` (`ID`, `hotel_id`, `room_type_id`, `room_name`, `room_no`, `rate_per_day`, `tax`, `total`, `seasonal_rate`, `floor_no`, `room_status_id`, `cleaning_status_id`, `facilities`, `image_url`) VALUES
(1, 2, 1, 'Room 1', '302', 3000, 100, 3100, 0, '5', 1, 1, '', 'images/room/20170404171056.jpg'),
(2, 2, 1, 'Room 2', '310', 5000, 500, 5500, 0, '6th', 1, 1, 'wifi', 'images/room/20170509170905.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_status`
--

CREATE TABLE `tbl_room_status` (
  `ID` int(11) NOT NULL,
  `room_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_status`
--

INSERT INTO `tbl_room_status` (`ID`, `room_status`) VALUES
(1, 'Ready to use'),
(2, 'Check-Out Rooms');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_type`
--

CREATE TABLE `tbl_room_type` (
  `ID` int(11) NOT NULL,
  `room_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_type`
--

INSERT INTO `tbl_room_type` (`ID`, `room_type`) VALUES
(1, 'A/C'),
(2, 'A/C Deluxe');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `hotel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `name`, `address`, `phone`, `email`, `hotel_id`) VALUES
(1, 'Admin', '', '', '', 0),
(2, 'Staff', '234westway', '23223232', 'staff1@gmail.com', 2),
(3, 'Staff 3', '565', '89898', 'st3@yahoo.com', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_cleaning_status`
--
ALTER TABLE `tbl_cleaning_status`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_customer_details`
--
ALTER TABLE `tbl_customer_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_food`
--
ALTER TABLE `tbl_food`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_hotel`
--
ALTER TABLE `tbl_hotel`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_package`
--
ALTER TABLE `tbl_package`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_package_booking`
--
ALTER TABLE `tbl_package_booking`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_package_image`
--
ALTER TABLE `tbl_package_image`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_room`
--
ALTER TABLE `tbl_room`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_room_status`
--
ALTER TABLE `tbl_room_status`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_room_type`
--
ALTER TABLE `tbl_room_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_cleaning_status`
--
ALTER TABLE `tbl_cleaning_status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_customer_details`
--
ALTER TABLE `tbl_customer_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_food`
--
ALTER TABLE `tbl_food`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_hotel`
--
ALTER TABLE `tbl_hotel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_package`
--
ALTER TABLE `tbl_package`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_package_booking`
--
ALTER TABLE `tbl_package_booking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_package_image`
--
ALTER TABLE `tbl_package_image`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_room`
--
ALTER TABLE `tbl_room`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_room_status`
--
ALTER TABLE `tbl_room_status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_room_type`
--
ALTER TABLE `tbl_room_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
