<?php 
	$page_id=3;
	include('includes/header.php'); 
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg" />
    </div>
	<?php 
        include('includes/menu.php'); 
    ?>
    <div class="container">
    	<div class="inner_page_mainheading">
            <h1>Booking</h1>
            <hr>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 booking-main">
        	<div class="booking_form">
                <form action="booking-search.php" method="POST">
                	<div class="row">
                        <div class="col-lg-12">
                            <label style="margin:0">Select Place<span class="valid">*</span></label>
                            <select name="place" required>
                                <option value="">Select</option>
                                <?php
                                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                                $db->connect();
                                $placeRes = $db->query("SELECT place FROM `".TABLE_HOTEL."`");
                                if (mysql_num_rows($placeRes) > 0) {
                                    while ($placeRow = mysql_fetch_array($placeRes)) {
                                        ?>
                                        <option value="<?= $placeRow['place']; ?>"><?= $placeRow['place']; ?></option>
                                        <?php
                                    }
                                }
                                $db->close();
                                ?>
                            </select>
                            <!-- <input type="date" placeholder="Check in Date">-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        	<label style="margin:0">Check in date</label>
                            <input class="form-control" id="demo" name="checkInDate" type="text" placeholder="Check in date" readonly="readonly">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        	<label style="margin:0">Check out date</label>
                            <input class="form-control" id="demo1" type="text" name="checkOutDate" placeholder="Check out date" readonly="readonly">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        	<label style="margin:0">No of adults</label>
                            <input class="form-control" style="margin-right: 5px;" id="adult_no" type="number" name="adult_no" placeholder="Adult"
                                   min="1">

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        	<label style="margin:0">No of children</label>
                            <input class="form-control" id="kids_no" type="number" name="kids_no" placeholder="Children" min="0">
                        </div>
                    </div>

                    <div class="row">
                    	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 booking_bottn">
                    		<input type="submit" value="Search">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php 
	include('includes/footer.php'); 
?>