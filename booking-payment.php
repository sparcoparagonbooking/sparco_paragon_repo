<?php
ob_start();
$page_id = 3.2;
include('includes/header.php');
if (!isset($_SESSION['customer_id'])) {
    header('Location: login.php');
}
if (!isset($_REQUEST['rooms']) && !@$_SESSION['booking']) {
    header('Location: index.php');
} else
    {
    $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    $db->connect();
    $checkInDate = date('d-m-Y');
    $checkOutDate = date('d-m-Y');
    $kidNo = 0;
    $adultNo = 1;
    if (isset($_POST['booking']) || isset($_SESSION['booking'])) {
        $roomDetailsArray = array();
        if(isset($_SESSION['booking'])){
            $booking = $_SESSION['booking'];
            $checkInDate = $App->dbformat_date($booking['checkInDate']);
            $checkOutDate = $App->dbformat_date($booking['checkOutDate']);
            $adultNo = $booking['adultNo'];
            $kidNo = $booking['kidNo'];
            $numRooms = $booking['numOfRooms'];
            $roomDetailsArray = unserialize($booking['rooms']);
        }
        if(isset($_REQUEST['numRooms'])){
            $numRooms = @mysql_escape_string($_REQUEST['numRooms']);
        }
        if(isset($_POST['rooms']) && $_POST['rooms']){
            $roomDetailsArray = unserialize($_POST['rooms']);
            $roomDetails = @mysql_escape_string($_POST['rooms']);
        }
        if (isset($_POST['checkInDate']) && $_POST['checkInDate']) {
            $checkInDate = $App->dbformat_date(@mysql_escape_string($_POST['checkInDate']));
        }
        if (isset($_POST['checkOutDate']) && $_POST['checkOutDate']) {
            $checkOutDate = $App->dbformat_date(@mysql_escape_string($_POST['checkOutDate']));
        }
        if (isset($_POST['kids_no']) && $_POST['kids_no']) {
            $kidNo = $_POST['kids_no'];
        }
        if (isset($_POST['adult_no']) && $_POST['adult_no']) {
            $adultNo = $_POST['adult_no'];
        }
        $bookingArray = array(

            "checkInDate"=>$checkInDate,
            "checkOutDate"=>$checkOutDate,
            "kidNo"=>$kidNo,
            "adultNo"=>$adultNo,
            "numOfRooms"=>$numRooms,
            "rooms"=>serialize($roomDetailsArray)
        );
        $_SESSION['booking']=$bookingArray;

        /***************** for getting user ******************/
        $customerId = $_SESSION['customer_id'];

        $customerQry = "SELECT * FROM ".TABLE_CUSTOMER." WHERE ID ='$customerId'";
        $customerRes = mysql_query($customerQry);
        $customerRow = mysql_fetch_array($customerRes);

        ?>

        <div class="inner_banner">
            <img src="images/booking_banner.jpg"/>
        </div>
        <?php
        include('includes/menu.php');
        ?>
        <div class="container">
            <div class="inner_page_mainheading">
                <h1>Booking</h1>
                <hr>
            </div>
            <div class="row booking_inner">

                <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                    <form id="msform" name="customerData" method="POST" action="payment_submission.php">
                        <input type="hidden" name="merchant_id" value="125143"/>
                        <input type="hidden" name="tid" id="tid" readonly value="<?php echo date("YmdHis");?>" />
                        <input type="hidden" name="order_id" value="<?php echo date("YmdHis");?>"/>
                        <input type="hidden" name="amount" value="1.00"/>
                        <input type="hidden" name="currency" value="INR"/>
                        <input type="hidden" name="redirect_url" value="http://paragonbooking.com/payment_result.php"/>
                        <input type="hidden" name="cancel_url" value="http://paragonbooking.com/payment_result.php"/>
                        <input type="hidden" name="language" value="EN"/>
                        <input type="hidden" name="integration_type" value="iframe_normal"/>

                        <!--******************************* Optional - Dummy data *****************************/-->
                        <input type="hidden" name="billing_name" value="<?php echo $customerRow['name']?>"/>
                        <input type="hidden" name="billing_address" value="<?php echo $customerRow['address']?>"/>
                        <input type="hidden" name="billing_city" value="Indore"/>
                        <input type="hidden" name="billing_state" value="MP"/>
                        <input type="hidden" name="billing_zip" value="<?php echo $customerRow['pin']?>"/>
                        <input type="hidden" name="billing_country" value="India"/>
                        <input type="hidden" name="billing_tel" value="<?php echo $customerRow['contact_no']?>"/>
                        <input type="hidden" name="billing_email" value="<?php echo $customerRow['email']?>"/>
                        <!-- /******************************** Optional - Dummy data Ends ************************/-->

                        <fieldset>
                            <h2 class="fs-title">Confirm Booking</h2>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 booking_search_item">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding:0">
                                        <img src="<?= $roomDetailsArray[0]['image_url']; ?>"/>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-top:10px">
                                        <h4><?= $roomDetailsArray[0]['hotel_name']; ?></h4>
                                        <div class="form-group">
                                            Place : <?= $roomDetailsArray[0]['place']; ?>
                                        </div>
                                        <div class="form-group">
                                            Room type : <?= $roomDetailsArray[0]['room_type']; ?>
                                        </div>
                                        <div class="form-group">
                                            Check In Date : <?= $checkInDate ?>
                                        </div>
                                        <div class="form-group">
                                            Check Out Date : <?= $checkOutDate ?>
                                        </div>
                                        <div class="form-group">
                                            No of Adults :
                                            <?= $adultNo; ?>
                                        </div>
                                        <div class="form-group">
                                            No of Children: <?= $kidNo; ?>
                                        </div>
                                        <div class="form-group">
                                            Number of rooms : <?= $numRooms ?>
                                        </div>
                                        <div class="form-group">
                                            Rate per Room : <?= $roomDetailsArray[0]['rate_per_day'] ?>
                                        </div>
                                        <div class="rate">
                                            <div class="left_sectn" data-rate="<?= ($roomDetailsArray[0]['rate_per_day']*$numRooms); ?>">Rate :
                                                <span>&#x20B9; <?= ($roomDetailsArray[0]['rate_per_day']*$numRooms); ?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </fieldset>

                    </form>
                </div>
            </div>
        </div>


        <?php
        $db->close();
    }else{
        header('Location: index.php');
    }
}
include('includes/footer.php');
?>