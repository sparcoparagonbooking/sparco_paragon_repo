/*jslint browser: true*/
/*global $, jQuery, alert*/
$(window).load(function () {
    'use strict';
    // Session Message
    $('#session_modal').modal('show');
    $('.session_modal_success .modal-title').html("Successfull");

});
$(function () {
    'use strict';
	$('.nav_trigger').click(function() {
        $('.main_menu').slideToggle();
    });
    'use strict';
    $('.cat_trigger').click(function () {
        var curTrigger = $(this),
            targetDiv = $('.categories'),
            flag = 0;
        targetDiv.slideToggle(300, function () {
            if (targetDiv.is(':hidden')) {
                curTrigger.find('.fa').removeClass('fa-minus').addClass('fa-plus');
            } else {
                curTrigger.find('.fa').removeClass('fa-plus').addClass('fa-minus');
            }
        });
    });
	$('.feedback_button').click(function() {
       $('.feedback_body').slideToggle();
   });
   $('.feedback_body span').click(function() {
       $('.feedback_body').slideToggle();
   });
    $('.ui_calander').datetimepicker({
        format: 'd-m-Y',
        step: 30,
        minDate: '0',
        timepicker: false,
        defaultDate: new Date()
    });
});
