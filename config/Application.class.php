<?php
session_start();
date_default_timezone_set('Asia/Kolkata'); 
class Application {
//Function : To encrypt and decrept string
//-
FUNCTION ENCRYPT_DECRYPT($Str_Message) {
//Function : encrypt/decrypt a string message v.1.0  without a known key
//Author   : Aitor Solozabal Merino (spain)
//Email    : aitor-3@euskalnet.net
//Date     : 01-04-2005
    $Len_Str_Message=STRLEN($Str_Message);
    $Str_Encrypted_Message="";
    FOR ($Position = 0;$Position<$Len_Str_Message;$Position++){
        // long code of the function to explain the algoritm
        //this function can be tailored by the programmer modifyng the formula
        //to calculate the key to use for every character in the string.
        $Key_To_Use = (($Len_Str_Message+$Position)+1); // (+5 or *3 or ^2)
        //after that we need a module division because can�t be greater than 255
        $Key_To_Use = (255+$Key_To_Use) % 255;
        $Byte_To_Be_Encrypted = SUBSTR($Str_Message, $Position, 1);
        $Ascii_Num_Byte_To_Encrypt = ORD($Byte_To_Be_Encrypted);
        $Xored_Byte = $Ascii_Num_Byte_To_Encrypt ^ $Key_To_Use;  //xor operation
        $Encrypted_Byte = CHR($Xored_Byte);
        $Str_Encrypted_Message .= $Encrypted_Byte;
       
        //short code of  the function once explained
        //$str_encrypted_message .= chr((ord(substr($str_message, $position, 1))) ^ ((255+(($len_str_message+$position)+1)) % 255));
    }
    RETURN $Str_Encrypted_Message;
} //end function

// Function : To capitalize a word 
//-

function capitalize($str)
{
	$str	=	stripslashes($str);
   	$str	=	trim($str);
	$str	=	strtolower($str);
	$str	=	ucwords($str);
	return $str;
}

//Function : To stripslashes, trim
//-
function convert($str)
 {
   $str1=stripslashes($str);
   $str2=trim($str1);
   return $str2;
 }

//Function : To stripslashes, mysql_real_escape_string, trim used for login
//-
function convertLogin($str)
 {
   $str1	=	stripslashes($str);
   $str2	=	trim($str1);
   $str3	=	mysql_real_escape_string($str2);
   $str4	=	htmlentities($str2);
   
   return $str4;
 } 
 
//Function : To split large sentence F
//-
function split_sentence($str,$count)
{
  if(strlen($str)>$count)
  {
   $str_content = substr($str, 0, $count);
   $str_pos = strrpos($str_content, " ");
   if ($str_pos>0)
    {
      $str_content = substr($str_content, 0, $str_pos);
      return $str_content."..."; 
	} 
   }
   else
   {
    return $str;
   }	
}
 
 
function showDate($dateTime, $format = "D, d-m-Y h:i A")
{
	$dateTime	=	strtotime($dateTime);
	return date($format,$dateTime);
}

function dbformat_date($exp_date) 
{	 
	/*$exp_date1=explode("-",$exp_date);
	$exp_date_original=$exp_date1[2]."-".$exp_date1[1]."-".$exp_date1[0];
	return $exp_date_original;*/
	$exp_date_original=$exp_date;
	
	  $exp_date1=explode("/",$exp_date);
	  if(count($exp_date1)==3)
	  {
	  	$exp_date_original=$exp_date1[2]."-".$exp_date1[1]."-".$exp_date1[0];
	  }
	  else
	  {
		$exp_date1=explode("-",$exp_date);
		$exp_date_original=$exp_date1[2]."-".$exp_date1[1]."-".$exp_date1[0];
	  }
	return $exp_date_original;
} 

function dbformat_time($exp_time) 
{
  $time =  date("H:i:s", strtotime($exp_time));
  return $time;
}

	// Image validation
	function validateImages($xtn) {
		$xtnArray = array("jpg", "png", "JPG", "jpeg", "JPEG");
		if (in_array($xtn, $xtnArray)) {
			return true;
		} else {
			return false;
		}
	}

//Function : To crope image thumb
//-

function cropImage($w,$h,$filename,$stype,$dest)
{

$filename = $filename;

$width = $w;
$height = $h;
switch($stype) {
        case 'gif':
        $simg = imagecreatefromgif($filename);
        break;
        case 'jpg':
        $simg = imagecreatefromjpeg($filename);
        break;
        case 'png':
        $simg = imagecreatefrompng($filename);
        break;
		
    }

header('Content-type: image/jpeg');

list($width_orig, $height_orig) = getimagesize($filename);

$ratio_orig = $width_orig/$height_orig;

if ($width/$height > $ratio_orig) 
  {
   $width = $height*$ratio_orig;
  } 
  else 
 {
   $height = $width/$ratio_orig;
}


$image_p = imagecreatetruecolor($width, $height);

imagecopyresampled($image_p, $simg, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

imagejpeg($image_p,$dest, 100);
}

// function for creating session message
function sessionMsgCreate($type, $msg)
{
    if($type == "success"){
        return '<div id="session_modal" class="modal fade session_modal_success session_modal" role="dialog"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Successful</h4></div><div class="modal-body"><p>'.$msg.'</p></div><div class="modal-footer"><button type="button" class="btn btn-success" data-dismiss="modal">Close</button></div></div></div></div>';
    }
    elseif ($type == "error")
    {
        return '<div id="session_modal" class="modal fade session_modal_error session_modal" role="dialog"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Error</h4></div><div class="modal-body"><p>'.$msg.'</p></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></div></div></div></div>';
    }
}
/**
* function for  validating whether the updating files are image or not 
* @pass the image name
* @return true if image format false if other formats
*/

	function imageValidation($path, $tempPath)
	{
		$path_info 	=	pathinfo($path);
		$ext	 	=	$path_info["extension"];
		$allowed 	=  	array("jpg", "JPG", "png", "PNG", "jpeg", "JPEG");		// allowed image extensions
		// Checking by type
		$allowedType = array(2, 3);
		if (in_array(exif_imagetype($tempPath), $allowedType) && in_array($ext,$allowed)) {
			return true;
		} else {
			return false;
		}
	}



/**
   * returns the number in word
   * Converting Currency Numbers to words currency format
   */
   function amountToWord($amount)
   {
	   $number = $amount;
	   $no = round($number);
	   $point = round($number - $no, 2) * 100;
	   $hundred = null;
	   $digits_1 = strlen($no);
	   $i = 0;
	   $str = array();
	   $words = array('0' => '', '1' => 'one', '2' => 'two',
	    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
	    '7' => 'seven', '8' => 'eight', '9' => 'nine',
	    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
	    '13' => 'thirteen', '14' => 'fourteen',
	    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
	    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
	    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
	    '60' => 'sixty', '70' => 'seventy',
	    '80' => 'eighty', '90' => 'ninety');
	   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
	   while ($i < $digits_1) {
	     $divider = ($i == 2) ? 10 : 100;
	     $number = floor($no % $divider);
	     $no = floor($no / $divider);
	     $i += ($divider == 10) ? 1 : 2;
	     if ($number) {
	        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
	        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
	        $str [] = ($number < 21) ? $words[$number] .
	            " " . $digits[$counter] . $plural . " " . $hundred
	            :
	            $words[floor($number / 10) * 10]
	            . " " . $words[$number % 10] . " "
	            . $digits[$counter] . $plural . " " . $hundred;
	     } else $str[] = null;
	  }
	  $str = array_reverse($str);
	  $result = implode('', $str);
	  $points = ($point) ?
	    "." . $words[$point / 10] . " " . 
	          $words[$point = $point % 10] : '';
	  $paise	=	'';
	  if($points)
	  {
	  	$paise	=	$points . " Paise";
	  }
	  return $result . "Rupees  " . $paise ." Only";
   }

	// URL Validation
	function validateURL ($urlInput) {
		$URLRegX = '/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&amp;?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/';
		if (filter_var($urlInput, FILTER_VALIDATE_URL)) {
			return true;
		} else {
			return false;
		}
	}


};

$App = new Application();
//$App->path 	=	'http://localhost/dental';
//$App->path 	=	'http://moopans.dentricz.com



?>