    <footer>
        <div class="container">
            <div class="foot_nav">
                    <ul>
                        <li><a href="index.php">Home</a> |</li>
                        <li><a href="about.php">About us</a> |</li>
                        <li><a href="booking.php">Booking</a> |</li>
                        <li><a href="facilities.php">Facilities</a> |</li>
                        <li><a href="package.php">Packages</a> |</li>
                        <li><a href="gallery.php">Gallery</a> |</li>
                        <!--<li><a href="restaurant.php">Restaurant</a> |</li>-->
                        <li><a href="contact.php">Contact us</a></li>
                        <?php
                        if (isset($_SESSION['log_id']) && $_SESSION['log_id']) {
                            ?>
                            | <li><a href="logout_blade.php">Logout</a></li>
                            <?php
                        } else {
                            ?>
                            | <li><a href="login.php">Login</a></li>
                            <?php
                        }
                        ?>
                    </ul>
            </div>
            <div class="copy">
                <p>© 2016 Paragon , All Rights Reserved.</p> <span>Powered by<a href="http://bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png" alt="bodhi"></a></span>
            </div>
        </div>
    </footer>
    <div id="error_pop" class="modal fade session_modal_error session_modal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Error</h4></div>
                <div class="modal-body"><p></p></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <?php if($page_id==1){ ?>
    <script type="text/javascript" src="js/amazingslider.js"></script>
    <script type="text/javascript" src="js/amazingcarousel.js"></script>    
    <script type="text/javascript" src="js/initslider-1.js"></script>
    <script type="text/javascript" src="js/initslider-2.js"></script>
    <script type="text/javascript" src="js/initslider-3.js"></script>
    <script type="text/javascript" src="js/initcarousel-4.js"></script>
    <script type="text/javascript" src="js/dcalendar.picker.js"></script>
    <script>
        $('#demo').dcalendarpicker({
            format: 'dd-mm-yyyy'
        });
        //$('#calendar-demo').dcalendar(); //creates the calendar
        $('#demo1').dcalendarpicker({
            format: 'dd-mm-yyyy'
        });
        //$('#calendar-demo1').dcalendar(); //creates the calendar
    </script>
    <?php } ?>
    <?php if($page_id==3){ ?>
    <script type="text/javascript" src="js/dcalendar.picker.js"></script>
    <script>
        $('#demo').dcalendarpicker({
            format: 'dd-mm-yyyy'
        });
        //$('#calendar-demo').dcalendar(); //creates the calendar
        $('#demo1').dcalendarpicker({
            format: 'dd-mm-yyyy'
        });
        //$('#calendar-demo1').dcalendar(); //creates the calendar
    </script>
    <?php } ?>
    <?php if($page_id==3.2 || $page_id==5.2){ ?>
	<script src="js/jquery.easing.min.js" type="text/javascript"></script> 
    <script>
    $(function() {
    
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    
    $(".next").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        
        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale('+scale+')'});
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });
    
    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });
    
    $(".submit").click(function(){
        return false;
    })
    
    });
    </script>
	<?php } ?>
    <?php if($page_id==4){ ?>
    <script src="js/pinterest_grid.js"></script> 
	<script>
            $(document).ready(function() {
    
                $('#blog-landing').pinterest_grid({
                    no_columns: 4,
                    padding_x: 10,
                    padding_y: 10,
                    margin_bottom: 50,
                    single_column_breakpoint: 700
                });
    
            });
    </script>
	<?php } ?>    
	<?php if($page_id==5.1){ ?>
        <script type="text/javascript" src="js/amazingslider.js"></script>
        <script type="text/javascript" src="js/initslider-5.js"></script>
        
    <?php } ?>    
    <?php if($page_id==6){ ?>
    <!--<script src="index_files/vlb_engine/jquery.min.js" type="text/javascript"></script>-->
	<script src="js/visuallightbox.js" type="text/javascript"></script>
	<script src="js/thumbscript1.js" type="text/javascript"></script>
	<script src="js/vlbdata1.js" type="text/javascript"></script>
    <?php } ?>
    <?php if($page_id==8){ ?>
	<script type="text/javascript" src="js/jquery.accordion.js"></script>
    <script type="text/javascript">
		$(function() {
			$('.example1').accordion({ multiOpen: false });
		});
	</script>
	<?php } ?>
    <script src="xdsoft_date/jquery.datetimepicker.full.min.js"></script>
</html>