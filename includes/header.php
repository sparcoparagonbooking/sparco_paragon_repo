<?php
require("config/config.inc.php");
require("config/Database.class.php");
require("config/Application.class.php");
?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sparco</title>
    <link rel="icon" href="images/x_icon.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="xdsoft_date/jquery.datetimepicker.min.css">
    <?php if($page_id==1){?>
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/amazingslider-1.css">
    <link rel="stylesheet" type="text/css" href="css/amazingslider-3.css">
    <link rel="stylesheet" type="text/css" href="css/amazingslider-2.css">
    <link rel="stylesheet" type="text/css" href="css/initcarousel-4.css">
    <link rel="stylesheet" type="text/css" href="css/dcalendar.picker.css">
    <style>input::-webkit-input-placeholder {color: #fff !important;} .fa-check-square-o:before {color:#fff;padding-right:10px} .feedback_body input::-webkit-input-placeholder { color:#a9a9a9 !important} }</style>
    <!--<script src="js/jquery-2.1.4.min.js"></script>-->
    <?php } ?>
    <?php if($page_id==3){ ?>
    <link rel="stylesheet" type="text/css" href="css/dcalendar.picker.css">
	<style>.datepicker{margin-bottom:0 !important; width: 100%;}</style>
    <?php } ?>
    <?php if($page_id==3.2){ ?>
    <link rel="stylesheet" type="text/css" href="css/dcalendar.picker.css">
	<style>.datepicker{margin-bottom:0 !important; width: 100%;}</style>
    <?php } ?>
    <?php if($page_id==3.2 || $page_id==5.2){ ?>
    <style> .booking_search_item{ text-align:left;} .booking_inner h4 { margin-left:0px} .booking_search_item p {font-size:14px;line-height:30px;}
    .booking_search_item .rate .left_sectn {float: right;}
    </style>
	<?php } ?>
    <?php if($page_id==5.1){ ?> 
        <link rel="stylesheet" type="text/css" href="css/amazingslider-5.css">
    <?php } ?>    
    <?php if($page_id==6){ ?>
	<link rel="stylesheet" href="css/vlightbox1.css" type="text/css" />
	<link rel="stylesheet" href="css/visuallightbox.css" type="text/css" media="screen" />
    <?php } ?>
    
</head>
<body>
<?php
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <div class="red_row"></div>
    <header>
        <div class="container">
            <div class="logo">
                <a href="index.php"><img src="images/logo.png" alt="paragon"></a>
            </div>
            <div class="social">
                <a href="#"><span class="goog"><i class="fa fa-google-plus"></i></span></a>
                <a href="#"><span class="twitt"><i class="fa fa-twitter"></i></span></a>
                <a href="#"><span class="faceb"><i class="fa fa-facebook-f"></i></span></a>
            </div>
            <div class="mob_num">
                <span><i class="fa fa-phone"></i></span>
                <a href="tel:+918023571341"><h3>+91 8023571341</h3></a>
            </div>
        </div>
    </header>
