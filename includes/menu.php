        <span class="nav_trigger"><i class="fa fa-bars"></i></span> 
        <div class="main_menu">
            <div class="container">
                <nav class="main_nav">
                    <ul>
                        <li><a <?php if($page_id==1){ echo 'class="active"'; } ?> href="index.php">Home</a></li>
                        <li><a <?php if($page_id==2){ echo 'class="active"'; } ?> href="about.php">About us</a></li>
                        <li><a <?php if($page_id==3||$page_id==3.1||$page_id==3.2){ echo 'class="active"'; } ?> href="booking.php">Booking</a></li>
                        <li><a <?php if($page_id==4){ echo 'class="active"'; } ?> href="facilities.php">Facilities</a></li>
                        <li><a <?php if($page_id==5 || $page_id==5.1 || $page_id==5.2){ echo 'class="active"'; } ?> href="package.php">Packages</a></li>
                        <li><a <?php if($page_id==6){ echo 'class="active"'; } ?> href="gallery.php">Gallery</a></li>
                       <!-- <li><a <?php /*if($page_id==7||$page_id==7.1){ echo 'class="active"'; } */?> href="restaurant.php">Restaurant</a></li>-->
                        <li><a <?php if($page_id==8){ echo 'class="active"'; } ?> href="contact.php">Contact us</a></li>
                        <?php
                        if (isset($_SESSION['log_id']) && $_SESSION['log_id']) {
                            ?>
                            | <li><a href="logout_blade.php">Logout</a></li>
                            <?php
                        } else {
                            ?>
                            | <li><a href="login.php">Login</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
        <!--feedback-->
        <div class="feedback_fourm">
           <div class="feedback_button">
               <h4>Feedback</h4></div>
           <div class="feedback_body">
               <span><i class="fa fa-times"></i></span>
               <h4>Feedback Fourm</h4>
               <input type="text" placeholder="Name" />
               <textarea placeholder="Feedback..."></textarea>
               <button>Submit</button>
           </div>
        </div>
        <!--ends feedback-->