<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");
if ($_SESSION['sparcoId'] == "") {
    header("location:../../logout.php");
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_REQUEST["roomStatus"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill all the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $data["room_status"] = $App->convert($_REQUEST["roomStatus"]);
            $success = $db->query_insert(TABLE_ROOM_STATUS, $data);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Room status added successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:index.php");
        break;
    case "edit" :
        if (!$_REQUEST['eid'] || !$_REQUEST["roomStatus"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $eid = $_REQUEST['eid'];
            if ($eid == 1) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Sorry. You can't edit the default room status!");
            } else {
                $success = 0;
                $data["room_status"] = $App->convert($_REQUEST["roomStatus"]);
                $success = $db->query_update(TABLE_ROOM_STATUS, $data, "ID = ".$eid);
                if ($success) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("success", "Details updated successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                }
            }
            $db->close();
        }
        header("location:edit.php?eid=".$eid);
        break;
    case "delete" :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = $_REQUEST['eid'];
            if ($eid == 1) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Sorry. You can't delete the default room status!");
            } else {
                $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db->connect();
                $success = 0;
                $success = @mysql_query("DELETE FROM `" . TABLE_ROOM_STATUS . "` WHERE ID = '{$eid}'");
                if ($success) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("success", "Room status deleted successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                }
                $db->close();
            }
        }
        header("location:index.php");
        break;
}