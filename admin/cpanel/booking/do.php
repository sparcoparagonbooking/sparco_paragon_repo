<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");
if ($_SESSION['sparcoId'] == "") {
    header("location:../../logout.php");
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_REQUEST["room"] || !$_REQUEST["name"]) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill all the details!");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            
            $success = 0;
            
            $data["name"] 			= $App->convert($_REQUEST["name"]);
            $data["address"]		= $App->convert($_REQUEST["address"]);
            $data["pin"] 			= $App->convert($_REQUEST["pin"]);
            $data["contact_no"] 	= $App->convert($_REQUEST["contact_no"]);
            $data["alt_contact_no"] = $App->convert($_REQUEST["alt_contact_no"]);
            $data["email"]			= $App->convert($_REQUEST["email"]);
            $data["remark"]			= $App->convert($_REQUEST["remark"]);
            
            $result 			= date("YmdHis");
            
            if ($_FILES["file"]["name"]) 
            {
                $path_info 	= pathinfo($_FILES["file"]["name"]);
                $ext 		= $path_info["extension"];
                if (!$App->imageValidation($_FILES["file"]["name"], $_FILES["file"]["tmp_name"])) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } 
                else 
                {
                    $fileName = $result . "." . $ext;
                    $img 	  = "";
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], "../../../images/customer/" . basename($fileName)))
                    {
                        $img = "images/customer/" . $fileName;
                    }
                    $data["photo"] = $img;
                }
            }
			if ($_FILES["doc_file"]["name"]) 
            {
                $path_info 	= pathinfo($_FILES["doc_file"]["name"]);
                $ext 		= $path_info["extension"];
                if (!$App->imageValidation($_FILES["doc_file"]["name"], $_FILES["doc_file"]["tmp_name"])) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } 
                else 
                {
                    $fileName = $result . "." . $ext;
                    $img 	  = "";
                    if (move_uploaded_file($_FILES["doc_file"]["tmp_name"], "../../../images/document/" . basename($fileName)))
                    {
                        $img = "images/document/" . $fileName;
                    }
                    $data["document_img"] = $img;
                }
            }
            $success = $db->query_insert(TABLE_CUSTOMER, $data);
            if ($success) 
            {
          		$success1	= 0;
          		$hotelId	= $db->userHotel($_SESSION['sparcoUserId']);
            
            	$data1["room_id"] 			= $App->convert($_REQUEST["room"]);
            	$data1["rate"] 				= $App->convert($_REQUEST["rate"]);
            	$data1["check_in_date"] 	= $App->dbformat_date($_REQUEST["check_in_date"]);
            	$data1["check_in_time"] 	= $App->convert($_REQUEST["check_in_time"]);
            	$data1["check_out_date"] 	= $App->dbformat_date($_REQUEST["check_out_date"]);
            	$data1["check_out_time"] 	= $App->convert($_REQUEST["check_out_time"]);
            	$data1["adult_no"] 			= $App->convert($_REQUEST["no_adult"]);
            	$data1["children_no"] 		= $App->convert($_REQUEST["no_child"]);
            	$data1["hotel_id"] 			= $App->convert($hotelId);
            	$data1["room_no"] 			= $App->convert($_REQUEST["no_room"]);
            	$data1["booking_type"] 		= 'office';
            	$data1["user_id"] 			= $_SESSION['sparcoUserId'];
            	$data1["customer_id"] 		= $success;
            	
            	$success1	=	$db->query_insert(TABLE_BOOKING,$data1);
            	if($success1)
            	{
            		$success2	=	0;
					$cu_Count	=	$_REQUEST['customer_row_count'];
					for($i=1;$i<=$cu_Count;$i++)
					{
						$name 		=	"name_".$i;
						$age 		=	"age_".$i;
						$gender 	=	"gender_".$i;
						$contact_no =	"contact_no_".$i;
						
						$data2['name']			=	$App->convert($_REQUEST[$name]);
						$data2['age']			=	$App->convert($_REQUEST[$age]);
						$data2['gender']		=	$App->convert($_REQUEST[$gender]);
						$data2['contact_no']	=	$App->convert($_REQUEST[$contact_no]);
						$data2['booking_id']	=	$success1;
						
						$success2	=	$db->query_insert(TABLE_CUSTOMER_DETAILS,$data2);
					}
					
				}
            	
            }
			if($success1)
            {
				$_SESSION['msg'] = $App->sessionMsgCreate("success", "Booking done successfully!");
			}
			else
			{
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
			}
            $db->close();
        }
        header("location:index.php");
        break;
        
    case "edit" :
        
        $bookingid 	= $_REQUEST['bookId'];
        $customerid = $_REQUEST['custId'];
        if (!$_REQUEST["room"] || !$_REQUEST["name"]) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill all the details!");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            
            $success = 0;
            
            $data["name"] 			= $App->convert($_REQUEST["name"]);
            $data["address"]		= $App->convert($_REQUEST["address"]);
            $data["pin"] 			= $App->convert($_REQUEST["pin"]);
            $data["contact_no"] 	= $App->convert($_REQUEST["contact_no"]);
            $data["alt_contact_no"] = $App->convert($_REQUEST["alt_contact_no"]);
            $data["email"]			= $App->convert($_REQUEST["email"]);
            $data["remark"]			= $App->convert($_REQUEST["remark"]);
            
            $result 			= date("YmdHis");
            
            if ($_FILES["file"]["name"]) 
            {
            	 // Delete the  photo first
	            $imgRes = $db->query("SELECT photo FROM `" . TABLE_CUSTOMER . "` WHERE ID = " . $customerid);
	            if (mysql_num_rows($imgRes) > 0) 
	            {
	                $imgRow = mysql_fetch_array($imgRes);
	                unlink("../../../" . $imgRow["photo"]);
	            }
            
                $path_info 	= pathinfo($_FILES["file"]["name"]);
                $ext 		= $path_info["extension"];
                if (!$App->imageValidation($_FILES["file"]["name"], $_FILES["file"]["tmp_name"])) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } 
                else 
                {
                    $fileName = $result . "." . $ext;
                    $img 	  = "";
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], "../../../images/customer/" . basename($fileName)))
                    {
                        $img = "images/customer/" . $fileName;
                    }
                    $data["photo"] = $img;
                }
            }
			if ($_FILES["doc_file"]["name"]) 
            {
            	// Delete the  photo first
	            $imgRes = $db->query("SELECT document_img FROM `" . TABLE_CUSTOMER . "` WHERE ID = " . $customerid);
	            if (mysql_num_rows($imgRes) > 0) 
	            {
	                $imgRow = mysql_fetch_array($imgRes);
	                unlink("../../../" . $imgRow["document_img"]);
	            }
	            
                $path_info 	= pathinfo($_FILES["doc_file"]["name"]);
                $ext 		= $path_info["extension"];
                if (!$App->imageValidation($_FILES["doc_file"]["name"], $_FILES["doc_file"]["tmp_name"])) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } 
                else 
                {
                    $fileName = $result . "." . $ext;
                    $img 	  = "";
                    if (move_uploaded_file($_FILES["doc_file"]["tmp_name"], "../../../images/document/" . basename($fileName)))
                    {
                        $img = "images/document/" . $fileName;
                    }
                    $data["document_img"] = $img;
                }
            }
            $success = $db->query_update(TABLE_CUSTOMER,$data," ID=$customerid");

      		$success1	= 0;
			$hotelId	= $db->userHotel($_SESSION['sparcoUserId']); 
			       
        	$data1["room_id"] 			= $App->convert($_REQUEST["room"]);
        	$data1["rate"] 				= $App->convert($_REQUEST["rate"]);
        	$data1["check_in_date"] 	= $App->dbformat_date($_REQUEST["check_in_date"]);
        	$data1["check_in_time"] 	= $App->convert($_REQUEST["check_in_time"]);
        	$data1["check_out_date"] 	= $App->dbformat_date($_REQUEST["check_out_date"]);
        	$data1["check_out_time"] 	= $App->convert($_REQUEST["check_out_time"]);
        	$data1["adult_no"] 			= $App->convert($_REQUEST["no_adult"]);
        	$data1["children_no"] 		= $App->convert($_REQUEST["no_child"]);
        	$data1["hotel_id"] 			= $App->convert($hotelId);
        	$data1["room_no"] 			= $App->convert($_REQUEST["no_room"]);
        	$data1["booking_type"] 		= 'office';
        	$data1["user_id"] 			= $_SESSION['sparcoUserId'];
        	$data1["customer_id"] 		= $customerid;
        	
        	$success1	=	$db->query_update(TABLE_BOOKING,$data1," ID=$bookingid ");
        	
    		$delete		=	mysql_query("DELETE FROM `".TABLE_CUSTOMER_DETAILS."` WHERE booking_id 	='{$bookingid}'");
    		
    		$success2	=	0;
			$cu_Count	=	$_REQUEST['customer_row_count'];
			for($i=1;$i<=$cu_Count;$i++)
			{
				$name 		=	"name_".$i;
				$age 		=	"age_".$i;
				$gender 	=	"gender_".$i;
				$contact_no =	"contact_no_".$i;
				
				$data2['name']			=	$App->convert($_REQUEST[$name]);
				$data2['age']			=	$App->convert($_REQUEST[$age]);
				$data2['gender']		=	$App->convert($_REQUEST[$gender]);
				$data2['contact_no']	=	$App->convert($_REQUEST[$contact_no]);
				$data2['booking_id']	=	$bookingid;
				
				$success2	=	$db->query_insert(TABLE_CUSTOMER_DETAILS,$data2);
			}
			
			if($success1||$success)
            {
				$_SESSION['msg'] = $App->sessionMsgCreate("success", "Booking updated successfully!");
			}
			else
			{
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
			}
            $db->close();
        }
        
        header("location:edit.php?eid=".$bookingid);
        break;
    case "delete" :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = $_REQUEST['eid'];
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;

            $bookQry	= $db->query_first("SELECT booking_type,customer_id FROM `" . TABLE_BOOKING . "` WHERE  ID = '{$eid}'");
            if($bookQry['booking_type']=='online'){
                $success 	= @mysql_query("DELETE FROM `" . TABLE_BOOKING . "` WHERE  ID = '{$eid}'");
            }
            elseif ($bookQry['booking_type']=='office'){
                // Delete the  image first
                $imgRes = $db->query("SELECT photo,document_img FROM `" . TABLE_CUSTOMER . "` WHERE ID = " . $bookQry['customer_id']);
                if (mysql_num_rows($imgRes) > 0) {
                    $imgRow = mysql_fetch_array($imgRes);
                    unlink("../../../" . $imgRow["photo"]);
                    unlink("../../../" . $imgRow["document_img"]);
                }

                $success2	= @mysql_query("DELETE FROM `" . TABLE_CUSTOMER_DETAILS . "` WHERE  booking_id = '{".$eid."}'");
                $success1 	= @mysql_query("DELETE FROM `" . TABLE_BOOKING . "` WHERE  ID = '{$eid}'");
                $success 	= @mysql_query("DELETE FROM `" . TABLE_CUSTOMER . "` WHERE ID = '{$bookQry['customer_id']}'");
            }

            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Booking deleted successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:index.php");
        break;
}