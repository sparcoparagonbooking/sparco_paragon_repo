<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Booking</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                        	<input type="text" name="check_in_date" id="" placeholder="Check in Date" value="<?php echo @$_REQUEST['check_in_date']; ?>" class="user_date">
                        	<input type="text" name="check_out_date" id="" placeholder="Check out Date" value="<?php echo @$_REQUEST['check_out_date']; ?>" class="user_date">
                            <input type="text" name="searchName" id="" placeholder="Room name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond	=	$cond." AND R.room_name LIKE '%".$_POST['searchName']."%'";
}
if(@$_REQUEST['check_in_date'])
{
	$cond	=	$cond." AND B.check_in_date LIKE '%".$App->dbformat_date($_POST['check_in_date'])."%'";
}
if(@$_REQUEST['check_out_date'])
{
	$cond	=	$cond." AND B.check_out_date LIKE '%".$App->dbformat_date($_POST['check_out_date'])."%'";
}
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Hotel Name</th>
                            <th>Room</th>
                            <th>Check in Date</th>
                            <th>Check Out Date</th>
                            <th>Number of Room</th>
                            <th>Booking Type</th>
                            <th>Customer Name</th>
                            <th>Staff in charge</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                   	$hotelId	= $db->userHotel($_SESSION['sparcoUserId']);
                    $i=0;
                    $selectAll = "SELECT B.ID,B.check_in_date,B.check_out_date,B.room_no,B.booking_type,
                    					 H.hotel_name,R.room_name,C.name cusName,U.name as staffName,C.ID as cusID
                    				FROM ".TABLE_BOOKING." B
                    		  INNER JOIN ".TABLE_CUSTOMER." C ON B.customer_id=C.ID
                    		  INNER JOIN ".TABLE_HOTEL." H ON B.hotel_id=H.ID
                    		   LEFT JOIN ".TABLE_ROOM." R ON B.room_id=R.ID
                    		   LEFT JOIN ".TABLE_USER." U ON B.user_id = U.ID
                    			   WHERE ".$cond." AND B.hotel_id=".$hotelId;
                    //echo $selectAll;die;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="10" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                        ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
	                       	<td><?= $row['hotel_name']; ?></td>
	                        <td><?= $row['room_name']; ?></td>
	                        <td><?= $App->dbformat_date($row['check_in_date']); ?></td>
	                        <td><?= $App->dbformat_date($row['check_out_date']); ?></td>
	                        <td><?= $row['room_no']; ?></td>
	                        <td><?= $row['booking_type']; ?></td>
	                        <td><?= $row['cusName']; ?></td>
	                        <td><?= $row['staffName']; ?></td>
	                        <td>
	                         <a class="show_table_lnk show_table_lnk_view" href="view.php?vid=<?php echo $row['ID'];?>">View</a>
	                        <a class="show_table_lnk show_table_lnk_edit" href="edit.php?eid=<?php echo $row['ID'];?>">Edit</a>
	                        <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?eid=<?php echo $row['ID'];?>&op=delete">Delete</a>
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>