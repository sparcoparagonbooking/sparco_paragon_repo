<?php
if (!@$_REQUEST['eid']) {
    header("location:index.php");
} else {
    $eid = $_REQUEST['eid'];
}
require('../admin_header.php');
if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <script xmlns="http://www.w3.org/1999/html">

        //delete row in index page

        function delete_type()
        {
            var del=confirm("Do you Want to Delete ?");
            if(del==true)
            {
                window.submit();
            }
            else
            {
                return false;
            }
        }
    </script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <?php
                    $pRes = $db->query("SELECT package_name FROM `".TABLE_PACKAGE."` WHERE ID = ".$eid);
                    if (mysql_num_rows($pRes) > 0) {
                        $pRow = mysql_fetch_array($pRes);
                        ?>
                        <h3>Images for - <?= $pRow['package_name']; ?></h3>
                        <?php
                    } else {
                        header("location:index.php");
                    }
                    ?>
                </div>
                <div class="page_controls">
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="#" data-toggle="modal" data-target="#package_image_pop">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    //echo $selectAll;die;
                    $selectAll = "SELECT * FROM `".TABLE_PACKAGE_IMAGE."` WHERE package_id = ".$eid;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
                        ?>
                        <tr><td colspan="3" align="center">There is no images for this package. </td></tr
                        <?php
                    }
                    else
                    {
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <tr>
                                <td><?php echo ++$i; ?></td>
                                <td><img class="view_thumb" src="../../../<?= $row['image_url']; ?>" alt="" /> </td>
                                <td>
                                    <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?eid=<?php echo $row['ID'];?>&op=imDelete&pid=<?= $eid; ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <div id="package_image_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="default_form" method="post" action="do.php?op=image&eid=<?= $eid; ?>" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Package Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form_block">
                        <label>Image (768x307) <span class="valid">*</span></label>
                        <input type="file" name="image" class="show_progress" id="image" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer2.php');
?>