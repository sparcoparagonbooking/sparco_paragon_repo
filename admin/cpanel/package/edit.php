<?php
if (!@$_REQUEST['eid']) {
    header("location:index.php");
} else {
    $editId = $_REQUEST['eid'];
}
require('../admin_header.php');
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit&eid=<?= $_REQUEST['eid']; ?>" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>Edit Package Details</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <?php
                        $res = $db->query("SELECT * FROM `".TABLE_PACKAGE."` WHERE ID = ".$editId);
                        if (mysql_num_rows($res) > 0) {
                            $row = mysql_fetch_array($res);
                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                                
                                <div class="form_block">
                                    <label>Package Name <span class="valid">*</span></label>
                                    <input type="text" name="package" value="<?= $row['package_name']; ?>">
                                </div>
                                <div class="form_block">
                                    <label>Hotel <span class="valid">*</span></label>
                                    <select name="hotelID" id="group" class="form-control2" required >
                                        <?php
                                        $categoryTypes="SELECT ".TABLE_HOTEL.".ID, ".TABLE_HOTEL.".hotel_name 
                                                              FROM `".TABLE_HOTEL."`
                                                              JOIN ".TABLE_USER." ON ".TABLE_HOTEL.".ID=".TABLE_USER.".hotel_id
                                                             WHERE ".TABLE_USER.".ID = ". $_SESSION['sparcoUserId']."";
                                        $res2=mysql_query($categoryTypes);	
                                        
                                        while($row1=mysql_fetch_array($res2))
                                        {?>	
                                            <option value="<?php echo $row1['ID']?>" <?php if($row['hotel_id']== $row1['ID']){?> selected="selected"<?php }?>><?php echo $row1['hotel_name']?></option>
                                        <?php 									
                                        }?>	            			
                                    </select>
                                </div>
                                <div class="form_block">
                                    <label>Days <span class="valid">*</span></label>
                                    <input type="text" name="days" value="<?= $row['day_no']; ?>" >
                                </div>
                                <div class="form_block">
                                    <label>Nights <span class="valid">*</span></label>
                                    <input type="text" name="nights" value="<?= $row['night_no']; ?>">
                                </div>
                                <div class="form_block">
                                    <label>Description </label>
                                    <textarea name="description"><?= $row['description']; ?></textarea>
                                </div>
                                <div class="form_block">
                                    <label>Rate <span class="valid">*</span></label>
                                    <input type="text" name="rate" value="<?= $row['rate']; ?>">
                                </div>
                                <div class="form_block">
                                    <label>Airport Transportation &nbsp;&nbsp;
                                    <input type="checkbox" name="airport" <?php if($row['airport_transportation']==true) { ?> checked <?php } ?> > </label>	
                                </div>
                                <div class="form_block">
                                    <label>Laundry service &nbsp;&nbsp;
                                    <input type="checkbox" name="laundry" <?php if($row['laundry_service']==true) { ?> checked <?php } ?>> </label>	
                                </div>
                                <div class="form_block">
                                    <label>Internet & wifi &nbsp;&nbsp;
                                    <input type="checkbox" name="wifi" <?php if($row['internet_wifi']==true) { ?> checked <?php } ?>> </label>	
                                </div>
                                <div class="form_block">
                                    <label>Break Fast &nbsp;&nbsp;
                                    <input type="checkbox" name="breakeFast" <?php if($row['break_fast']==true) { ?> checked <?php } ?>> </label>	
                                </div>
                                <div class="form_block">
                                    <label>Lunch &nbsp;&nbsp;
                                    <input type="checkbox" name="lunch" <?php if($row['lunch']==true) { ?> checked <?php } ?>> </label>	
                                </div>
                                <div class="form_block">
                                    <label>Dinner &nbsp;&nbsp;
                                    <input type="checkbox" name="dinner" <?php if($row['dinner']==true) { ?> checked <?php } ?>> </label>	
                                </div>
                                <div class="form_block">
                                    <label>Candle Light Dinner &nbsp;&nbsp;
                                    <input type="checkbox" name="candle" <?php if($row['candle_light_dinner']==true) { ?> checked <?php } ?>> </label>	
                                </div>
                                <div class="form_block">
                                    <label>Other Inclusion </label>
                                    <textarea name="inclutions" ><?= $row['other_inclusion']; ?></textarea>
                                </div>
                                <div class="form_block">
                                    <label>Exclusion </label>
                                    <textarea name="exclusions" ><?= $row['exclusion'];?></textarea>
                                </div>  
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
