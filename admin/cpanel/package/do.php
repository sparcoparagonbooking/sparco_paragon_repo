<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");
if ($_SESSION['sparcoId'] == "") {
    header("location:../../logout.php");
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_REQUEST["package"])
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill all the details!");
        }
        else
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $data["package_name"]           = $App->convert($_REQUEST["package"]);
            $data["night_no"]               = $App->convert($_REQUEST["nights"]);
            $data["day_no"]                 = $App->convert($_REQUEST["days"]);
            $data["hotel_id"]               = $App->convert($_REQUEST["hotelID"]);
            $data["description"]            = $App->convert($_REQUEST["description"]);
            $data["rate"]                   = $App->convert($_REQUEST["rate"]);
            if( isset($_REQUEST['airport'])){
                $data['airport_transportation']		=	1;
            }else{
                $data['airport_transportation']		=	0;
            }
            if( isset($_REQUEST['laundry'])){
                $data['laundry_service']		    =	1;
            }else{
                $data['laundry_service']		    =	0;
            }
            if( isset($_REQUEST['wifi'])){
                $data['internet_wifi']		        =	1;
            }else{
                $data['internet_wifi']		        =	0;
            }
            if( isset($_REQUEST['breakeFast'])){
                $data['break_fast']		            =	1;
            }else{
                $data['break_fast']		            =	0;
            }
            if( isset($_REQUEST['lunch'])){
                $data['lunch']		                =	1;
            }else{
                $data['lunch']		                =	0;
            }
            if( isset($_REQUEST['dinner'])){
                $data['dinner']		                =	1;
            }else{
                $data['dinner']		                =	0;
            }
            if( isset($_REQUEST['candle'])){
                $data['candle_light_dinner']		=	1;
            }else{
                $data['candle_light_dinner']		=	0;
            }
            $data["other_inclusion"]        = $App->convert($_REQUEST["inclutions"]);
            $data["exclusion"]              = $App->convert($_REQUEST["exclusions"]);
            $data["user_id"]                = $_SESSION['sparcoUserId'];

            $success = $db->query_insert(TABLE_PACKAGE, $data);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Package added successfully!");
                header("location:images.php?eid=$success");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                header("location:index.php");
            }
            $db->close();
        }

        break;
    case "edit" :
        if (!$_REQUEST['eid'] || !$_REQUEST["description"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $eid = $_REQUEST['eid'];
            $success = 0;

            $data["package_name"]           = $App->convert($_REQUEST["package"]);
            $data["night_no"]               = $App->convert($_REQUEST["nights"]);
            $data["day_no"]                 = $App->convert($_REQUEST["days"]);
            $data["hotel_id"]               = $App->convert($_REQUEST["hotelID"]);
            $data["description"]            = $App->convert($_REQUEST["description"]);
            $data["rate"]                   = $App->convert($_REQUEST["rate"]);
            if( isset($_REQUEST['airport'])){
                $data['airport_transportation']		=	1;
            }else{
                $data['airport_transportation']		=	0;
            }
            if( isset($_REQUEST['laundry'])){
                $data['laundry_service']		    =	1;
            }else{
                $data['laundry_service']		    =	0;
            }
            if( isset($_REQUEST['wifi'])){
                $data['internet_wifi']		        =	1;
            }else{
                $data['internet_wifi']		        =	0;
            }
            if( isset($_REQUEST['breakeFast'])){
                $data['break_fast']		            =	1;
            }else{
                $data['break_fast']		            =	0;
            }
            if( isset($_REQUEST['lunch'])){
                $data['lunch']		                =	1;
            }else{
                $data['lunch']		                =	0;
            }
            if( isset($_REQUEST['dinner'])){
                $data['dinner']		                =	1;
            }else{
                $data['dinner']		                =	0;
            }
            if( isset($_REQUEST['candle'])){
                $data['candle_light_dinner']		=	1;
            }else{
                $data['candle_light_dinner']		=	0;
            }
            $data["other_inclusion"]        = $App->convert($_REQUEST["inclutions"]);
            $data["exclusion"]              = $App->convert($_REQUEST["exclusions"]);
            $data["user_id"]                = $_SESSION['sparcoUserId'];

            $success = $db->query_update(TABLE_PACKAGE, $data, "ID = ".$eid);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Details updated successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:edit.php?eid=".$eid);
        break;
    case 'image' :
        if (!$_REQUEST['eid'] || !$_FILES['image']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to add image!");
        } else {
            $data['package_id'] = $App->convert($_REQUEST['eid']);
            $path_info = pathinfo($_FILES["image"]["name"]);
            $ext = $path_info["extension"];
            if (!$App->imageValidation($_FILES["image"]["name"], $_FILES["image"]["tmp_name"])) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
            } else {
                $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db->connect();
                $result = date("YmdHis");
                $fileName = $result . "." . $ext;
                $img = "";
                if (move_uploaded_file($_FILES["image"]["tmp_name"], "../../../images/package/" . basename($fileName))) {
                    $img = "images/package/" . $fileName;
                }
                $data["image_url"] = $img;
                $success = 0;
                $success = $db->query_insert(TABLE_PACKAGE_IMAGE, $data);
                if ($success) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("success", "Package image added successfully.");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                }
            }
            $db->close();
        }
        header("location:images.php?eid=".$_REQUEST['eid']);
        break;
    case 'imDelete' :
        if (!$_REQUEST['eid'] || !$_REQUEST['pid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = $_REQUEST['eid'];
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            // Delete package image
            $pImgRes = $db->query("SELECT image_url FROM `".TABLE_PACKAGE_IMAGE."` WHERE ID = ".$eid);
            if (mysql_num_rows($pImgRes) > 0) {
                while ($pImgRow = mysql_fetch_array($pImgRes)) {
                    unlink("../../../" . $pImgRow["image_url"]);
                }
            }
            $success = 0;
            $success = @mysql_query("DELETE FROM `".TABLE_PACKAGE_IMAGE."` WHERE ID = '{$eid}'");
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Package image deleted successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();

        }
        header("location:images.php?eid=".$_REQUEST['pid']);
        break;
    case 'delete' :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = $_REQUEST['eid'];
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            // Delete package image
            $pImgRes = $db->query("SELECT image_url FROM `".TABLE_PACKAGE_IMAGE."` WHERE package_id = ".$eid);
            if (mysql_num_rows($pImgRes) > 0) {
                while ($pImgRow = mysql_fetch_array($pImgRes)) {
                    unlink("../../../" . $pImgRow["image_url"]);
                }
            }
            $successa = 0;
            $successa = @mysql_query("DELETE FROM `".TABLE_PACKAGE_IMAGE."` WHERE package_id = '{$eid}'");
            if ($successa) {
                $success = @mysql_query("DELETE FROM `" . TABLE_PACKAGE . "` WHERE ID = '{$eid}'");
            }
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Package Details deleted successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();

        }
        header("location:index.php");
        break;
}