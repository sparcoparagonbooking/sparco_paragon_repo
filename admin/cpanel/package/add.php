<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form">
                <div class="bd_panel_head">
                    <h3>Add Packages</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                            <div class="form_block">
                                <label>Package Name <span class="valid">*</span></label>
                                <input type="text" name="package" required>
                            </div>
                            <div class="form_block">
                                <label>Hotel <span class="valid">*</span></label>
                                <select name="hotelID" id="group" class="form-control2" required >
                                    <?php
                                    $categoryTypes="SELECT ".TABLE_HOTEL.".ID, ".TABLE_HOTEL.".hotel_name 
                                                              FROM `".TABLE_HOTEL."`
                                                              JOIN ".TABLE_USER." ON ".TABLE_HOTEL.".ID=".TABLE_USER.".hotel_id
                                                             WHERE ".TABLE_USER.".ID = ". $_SESSION['sparcoUserId']."";
                                    $res2=mysql_query($categoryTypes);
                                    while($row=mysql_fetch_array($res2))
                                    {?>	
                                        <option value="<?php echo $row['ID']?>"><?php echo $row['hotel_name']?></option>
                                    <?php 									
                                    }?>	            			
                                </select>
                            </div>
                            <div class="form_block">
                                <label>Days <span class="valid">*</span></label>
                                <input type="text" name="days" required>
                            </div>
                            <div class="form_block">
                                <label>Nights <span class="valid">*</span></label>
                                <input type="text" name="nights" required>
                            </div>
                            <div class="form_block">
                                <label>Description </label>
                                <textarea name="description" ></textarea>
                            </div>
                            <div class="form_block">
                                <label>Rate <span class="valid">*</span></label>
                                <input type="text" name="rate" required>
                            </div>
                            <div class="form_block">
                                <label>Airport Transportation &nbsp;&nbsp; </label>
                                <input type="checkbox" name="airport">
                            </div>
                             <div class="form_block">
                                <label>Laundry service &nbsp;&nbsp; </label>
                                 <input type="checkbox" name="laundry">
                            </div>
                            <div class="form_block">
                                <label>Internet & wifi &nbsp;&nbsp; </label>
                                <input type="checkbox" name="wifi">
                            </div>
                            <div class="form_block">
                                <label>Break Fast &nbsp;&nbsp; </label>
                                <input type="checkbox" name="breakeFast">
                            </div>
                            <div class="form_block">
                                <label>Lunch &nbsp;&nbsp; </label>
                                <input type="checkbox" name="lunch">
                            </div>
                            <div class="form_block">
                                <label>Dinner &nbsp;&nbsp; </label>
                                <input type="checkbox" name="dinner">
                            </div>
                            <div class="form_block">
                                <label>Candle Light Dinner &nbsp;&nbsp; </label>
                                <input type="checkbox" name="candle">
                            </div>
                            <div class="form_block">
                                <label>Other Inclusion </label>
                                <textarea name="inclutions" ></textarea>
                            </div>
                            <div class="form_block">
                                <label>Exclusion </label>
                                <textarea name="exclusions" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
