<?php
require('../admin_header.php');
if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Package</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <input type="text" name="searchName" id="" placeholder="Hotel / Package" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." AND h.hotel_name LIKE '%".$_POST['searchName']."%' or p.package_name LIKE '%".$_POST['searchName']."%'";
}

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Hotel</th>
                            <th>Package</th>
                            <th>Day & Night</th>
                            <th>Rate</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    //echo $selectAll;die;
                    $hotelId	= $db->userHotel($_SESSION['sparcoUserId']);
                    if($_SESSION['sparcoType']== 'staff')
                    {
                        $cond = $cond." AND p.hotel_id=$hotelId ";
                    }
                    $selectAll = "SELECT p.ID, p.package_name, p.night_no, p.day_no, p.description, p.airport_transportation, 
                                         p.laundry_service, p.internet_wifi, p.break_fast, p.lunch, p.dinner, 
                                         p.candle_light_dinner, p.other_inclusion, p.exclusion, p.rate, h.hotel_name, 
                                         u.name as userName 
                                    FROM `".TABLE_PACKAGE."` p 
                                    INNER JOIN `".TABLE_HOTEL."` h ON p.hotel_id = h.ID  
                                    INNER JOIN `".TABLE_USER."` u ON p.user_id = u.ID WHERE ".$cond;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="6" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                        ?>
                             <tr>
                                <td><?php echo ++$i; ?></td>
                                <td><?= $row['hotel_name']; ?></td>
                                <td><?= $row['package_name']; ?></td>
                                <td><?php echo $row['day_no']." Days &  ".$row['night_no']." Nights"; ?></td>
                                <td style="text-align: right;"><?= $row['rate']; ?></td>
                                <td>
                                    <a class="show_table_lnk show_table_lnk_view" href="#" data-toggle="modal" data-target="#package_view<?= $i; ?>">View</a>
                                    <a class="show_table_lnk show_table_lnk_edit" href="edit.php?eid=<?php echo $row['ID'];?>">Edit</a>
                                    <a href="images.php?eid=<?= $row['ID']; ?>" class="show_table_lnk show_table_lnk_edit show_table_lnk_view">Photos</a>
                                    <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?eid=<?php echo $row['ID'];?>&op=delete">Delete</a>
                                    <div id="package_view<?= $i; ?>" class="modal fade bd_modal" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Package Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped package_view_table">
                                                            <tbody>
                                                                <tr>
                                                                    <th>Hotel</th>
                                                                    <td><?= $row['hotel_name']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Package Name</th>
                                                                    <td><?= $row['package_name']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>No of Nights</th>
                                                                    <td><?= $row['night_no']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>No of Days</th>
                                                                    <td><?= $row['day_no']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Description</th>
                                                                    <td><?= $row['description']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Airport Transportation</th>
                                                                    <td><?= ($row['airport_transportation']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Laundry Service</th>
                                                                    <td><?= ($row['laundry_service']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Internet/WiFi</th>
                                                                    <td><?= ($row['internet_wifi']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Breakfast</th>
                                                                    <td><?= ($row['break_fast']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Lunch</th>
                                                                    <td><?= ($row['lunch']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Dinner</th>
                                                                    <td><?= ($row['dinner']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Candle Light Dinner</th>
                                                                    <td><?= ($row['candle_light_dinner']) ? '<i class="ion ion-checkmark-round text-success"></i>' : '<i class="ion ion-close-round text-danger"></i>' ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Other Inclusion</th>
                                                                    <td><?= $row['other_inclusion']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Rate</th>
                                                                    <td><?= $row['rate']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Created User</th>
                                                                    <td><?= $row['userName']; ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
	                        <?php
	                    }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>