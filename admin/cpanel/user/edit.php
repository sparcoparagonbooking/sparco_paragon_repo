<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
if (!$_REQUEST['eid']) {
    header('Location: index.php');
} else {
    $editId = @mysql_escape_string($_REQUEST['eid']);
}
$uRes = $db->query("SELECT * FROM `".TABLE_USER."` WHERE ID = '".$editId."'");
if (mysql_num_rows($uRes) > 0) {
    $uRow = mysql_fetch_array($uRes);
    //var_dump($uRow);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit&eid=<?= $editId; ?>" class="default_form">
                <div class="bd_panel_head">
                    <h3>Edit User Details</h3>
                </div>

                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Name <span class="valid">*</span></label>
                                <input type="text" name="name" value="<?= $uRow['name']; ?>" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Hotel</label>
                                <select name="hotel">
                                    <?php
                                    $hRes = $db->query("SELECT ID, hotel_name FROM `".TABLE_HOTEL."`");
                                    if (mysql_num_rows($hRes) > 0) {
                                        while ($hRow = mysql_fetch_array($hRes)) {
                                            ?>
                                            <option value="<?= $hRow['ID']; ?>" <?php if ($hRow['ID'] == $uRow['hotel_id']) { ?> selected <?php } ?>><?= $hRow['hotel_name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea name="address"><?= $uRow['address']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Contact No</label>
                                <input type="text" name="phone" value="<?= $uRow['phone']; ?>">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="email" name="email" value="<?= $uRow['email']; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
