<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form">
                <div class="bd_panel_head">
                    <h3>Add User</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Name <span class="valid">*</span></label>
                                <input type="text" name="name" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Hotel</label>
                                <select name="hotel">
                                    <?php
                                    $hRes = $db->query("SELECT ID, hotel_name FROM `".TABLE_HOTEL."`");
                                    if (mysql_num_rows($hRes) > 0) {
                                        while ($hRow = mysql_fetch_array($hRes)) {
                                            ?>
                                            <option value="<?= $hRow['ID']; ?>"><?= $hRow['hotel_name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea name="address"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Contact No</label>
                                <input type="text" name="phone">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="email" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>User Name<span class="valid">*</span></label>
                                <input type="text" name="userName" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Password <span class="valid">*</span></label>
                                <input type="password" name="password" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 form_block_row">
                            <div class="form_block">
                                <label>Confirm Password <span class="valid">*</span></label>
                                <input type="password" name="cPassword" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
