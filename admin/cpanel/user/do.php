<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");
if ($_SESSION['sparcoId'] == "") {
    header("location:../../logout.php");
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_REQUEST["name"] || !$_REQUEST["userName"] || !$_REQUEST["password"] || !$_REQUEST["cPassword"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill valid details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $data["name"] = $App->convert($_REQUEST["name"]);
            $data["address"] = $App->convert($_REQUEST["address"]);
            $data["phone"] = $App->convert($_REQUEST["phone"]);
            $data["email"] = $App->convert($_REQUEST["email"]);
            $data['hotel_id'] = $App->convert($_REQUEST['hotel']);
            $lData["username"] = $App->convert($_REQUEST["userName"]);
            $pass1 = $App->convert($_REQUEST['password']);
            $pass2 = $App->convert($_REQUEST['cPassword']);
            $userExistId = 0;
            if($data['email']){
                $userExistId = $db->existValuesId(TABLE_USER, "email = '".$data['email']."'");
            }

            $logExistId = 0;
            $logExistId = $db->existValuesId(TABLE_LOGIN, "username = '".$lData['username']."'");
            //echo $existId;
            if (!$userExistId && !$logExistId) {
                if ($pass1 == $pass2) {
                    $password = @mysql_real_escape_string(htmlentities($pass1));
                    $lData['password'] = md5($password);
                    $success = $db->query_insert(TABLE_USER, $data);
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Password mismatch! Please try again.");
                }
                if ($success) {
                    //$lData = array("userName" => $data['username'], "password" => $data['password'], 'usertype' => "staff", "user_id" => $success);
                    $lData['usertype'] = "staff";
                    $lData['user_id'] = $success;
                    $lSuccess = $db->query_insert(TABLE_LOGIN, $lData);
                    $_SESSION['msg'] = $App->sessionMsgCreate("success", "User added successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                }
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "User name or email already exist!");
            }
            $db->close();
        }

        header("location:index.php");
        break;
    case "edit" :
        if (!$_REQUEST["name"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $eid = @mysql_escape_string($_REQUEST['eid']);
            if ($eid == 1) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Sorry. You can't edit the details of super admin!");
            }
            else
            {
                $userExistId = 0;
                if($data['email']){
                    $userExistId = $db->existValuesId(TABLE_USER, "email = '".$data['email']."'");
                }
                if (!$userExistId) {
                    $success = 0;
                    $data["name"] = $App->convert($_REQUEST["name"]);
                    $data["address"] = $App->convert($_REQUEST["address"]);
                    $data["phone"] = $App->convert($_REQUEST["phone"]);
                    $data["email"] = $App->convert($_REQUEST["email"]);
                    $data['hotel_id'] = $App->convert($_REQUEST['hotel']);
                    $success = $db->query_update(TABLE_USER, $data, "ID = '" . $eid . "'");
                    if ($success) {
                        $_SESSION['msg'] = $App->sessionMsgCreate("success", "User details updated successfully!");
                    } else {
                        $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                    }
                }
                else{
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Email already exist!");
                }
            }
            $db->close();
        }
        header("location:index.php");
        break;
    case "delete" :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = @mysql_escape_string($_REQUEST['eid']);
            if ($eid == 1) {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Sorry. You can't delete super admin!");
            } else {
                $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db->connect();
                $success = 0;
                $success = @mysql_query("DELETE FROM `" . TABLE_LOGIN . "` WHERE user_id = '{$eid}' AND usertype='staff'");
                if ($success) {
                    $uSuccess = @mysql_query("DELETE FROM `" . TABLE_USER . "` WHERE ID = '{$eid}'");
                }
                if ($success && $uSuccess) {
                    $_SESSION['msg'] = $App ->sessionMsgCreate("success", "user deleted successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong! Please try again.");
                }
                $db->close();
            }
        }
        header("location:index.php");
        break;
}