<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>Add Gallery Image</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                            <div class="form_block">
                                <label>Food Name <span class="valid">*</span></label>
                                <input type="text" name="foodName" required>
                            </div>
                            <div class="form_block">
                                <label>Description <span class="valid">*</span></label>
                                <textarea name="description" required></textarea>
                            </div>
                            <div class="form_block">
                                <label>Image <span class="valid">*</span></label>
                                <input type="file" name="image" class="show_progress" id="image" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
