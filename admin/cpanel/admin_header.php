<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");

if($_SESSION['sparcoId']==""){
	header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sparco</title>
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="../../css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../../css/perfect-scrollbar.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.structure.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.theme.css">
	<link rel="stylesheet/less" type="text/css" href="../../css/timepicker.less">


	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<script src="../../js/jquery-2.2.1.min.js"></script>
	<script src="../../js/jquery-ui.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/perfect-scrollbar.jquery.js"></script>
	<script src="../../js/bootstrap-timepicker.js"></script>
	<script src="../../js/script.js"></script>

        <!--<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>-->

</head>
<body>
<div class="side_panel opened">
	<div class="app_logo">
		<img src="../../images/logo.png" alt="" /> <span>Sparco</span>
	</div>
	<div class="user_det">
            
	</div>
	<div class="side_nav_wrapper">
		<ul class="side_nav">
		<?php  if($_SESSION['sparcoType']== 'admin') 
		{?>
			
			<li>
				<a href="../user/">
					<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
					<span class="side_nav_text">User</span>
				</a>
			</li>
			<li>
				<a href="../hotel/">
					<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
					<span class="side_nav_text">Hotel</span>
				</a>
			</li>
			<li class="has_child">
				<a href="#" >
					<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
					<span class="side_nav_text">Report</span>
					<span class="side_nav_drop_ico"></span>
				</a>
				<ul class="side_sub_nav">
					<li>
						<a href="../report/customer.php">
							<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
							<span class="side_nav_text">Customer</span>
						</a>
					</li>
					<li>
						<a href="../report/office-booking.php">
							<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
							<span class="side_nav_text">Office Booking</span>
						</a>
					</li>
					<li>
						<a href="../report/online-booking.php">
							<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
							<span class="side_nav_text">Online Booking</span>
						</a>
					</li>
					<li>
						<a href="../report/package-booking.php">
							<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
							<span class="side_nav_text">Package Booking</span>
						</a>
					</li>
                                        <li>
                                                <a href="../report/payment.php">
                                                       <span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
                                                       <span class="side_nav_text">Payment</span>
                                                </a>
                                        </li>
                                       
				</ul>
			</li>
			
		<?php } if($_SESSION['sparcoType']== 'staff'){?>
			<li class="has_child">
				<a href="#" >
					<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
					<span class="side_nav_text">Room</span>
					<span class="side_nav_drop_ico"></span>
				</a>
				<ul class="side_sub_nav">
					<li>
						<a href="../room-type/">
							<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
							<span class="side_nav_text">Room Type</span>
						</a>
					</li>

					<li>
						<a href="../room-status">
							<span class="side_nav_ico"><i class="ion ion-ios-game-controller-a"></i></span>
							<span class="side_nav_text">Room Status</span>
						</a>
					</li>
					<li>
						<a href="../cleaning-status">
							<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
							<span class="side_nav_text">Cleaning Status</span>
						</a>
					</li>
					<li>
						<a href="../room/">
							<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
							<span class="side_nav_text">Add Room</span>
						</a>
					</li>
				</ul>
			</li>
            <li>
                <a href="../package/">
                    <span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
                    <span class="side_nav_text">Package</span>
                </a>
            </li>
			<li class="has_child">
				<a href="#" >
					<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
					<span class="side_nav_text">Booking</span>
					<span class="side_nav_drop_ico"></span>
				</a>
				<ul class="side_sub_nav">
					<li>
						<a href="../booking/">
							<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
							<span class="side_nav_text">Room Booking</span>
						</a>
					</li>
					<li>
						<a href="../package-booking/">
							<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
							<span class="side_nav_text">Package Booking</span>
						</a>
					</li>
				</ul>
			</li>
		<?php } ?>
			
			<li>
				<a href="../food/">
					<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
					<span class="side_nav_text">Food</span>
				</a>
			</li>
			<li>
				<a href="../gallery/">
					<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
					<span class="side_nav_text">Gallery</span>
				</a>
			</li>
			
		</ul>
	</div>
</div>
<div class="wrapper give_way">
<div class="header">
	<div class="container-fluid">
		<div class="nav_toggler">
			<span class="ion ion-android-menu"></span>
		</div>
		<div class="header_user_controls">
			<ul class="header_main_nav">
				<li class="has_child">
					<a href="#">
                        <?php
                        $staffQry = mysql_query("SELECT name From ".TABLE_USER." WHERE ID=".$_SESSION['sparcoUserId']."");
                        $staffRes = mysql_fetch_array($staffQry);
                        ?>
						<span class="nav_text"><?=$staffRes['name'];?></span>

						<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
						<div class="bd_clear"></div>
					</a>
					<ul class="header_sub_nav">
						<li><a href="../changePassword">Change Password</a></li>
						<li><a href="../../logout.php">Log Out</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="bd_clear"></div>
	</div>
</div>
<div class="container-fluid">