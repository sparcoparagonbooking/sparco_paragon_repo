<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");
if ($_SESSION['sparcoId'] == "") {
    header("location:../../logout.php");
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_FILES['image'] || !$_REQUEST["description"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill all the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $data["description"] = $App->convert($_REQUEST["description"]);
            $imageValidity = 1;
            if ($_FILES["image"]["name"]) {
                $path_info = pathinfo($_FILES["image"]["name"]);
                $ext = $path_info["extension"];
                if (!$App->imageValidation($_FILES["image"]["name"], $_FILES["image"]["tmp_name"])) {
                    $imageValidity = 0;
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } else {
                    $result = date("YmdHis");
                    $fileName = $result . "." . $ext;
                    $img = "";
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], "../../../images/gallery/" . basename($fileName))) {
                        $img = "images/gallery/" . $fileName;
                    }
                    $data["image_url"] = $img;
                }
            }
            if ($imageValidity) {
                $success = $db->query_insert(TABLE_GALLERY, $data);
                if ($success) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("success", "Images added successfully!");
                } else {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
                }
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
            }
            $db->close();
        }
        header("location:index.php");
        break;
    case "edit" :
        if (!$_REQUEST['eid'] || !$_REQUEST["description"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $eid = $_REQUEST['eid'];
            $success = 0;
            $data["description"] = $App->convert($_REQUEST["description"]);
            if ($_FILES["image"]["name"]) {
                $path_info = pathinfo($_FILES["image"]["name"]);
                $ext = $path_info["extension"];
                if (!$App->imageValidation($_FILES["image"]["name"], $_FILES["image"]["tmp_name"])) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } else {
                    $result = date("YmdHis");
                    $fileName = $result . "." . $ext;
                    $img = "";
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], "../../../images/gallery/" . basename($fileName))) {
                        $img = "images/gallery/" . $fileName;
                        // Deleting the existing image
                        $imgRes = mysql_query("SELECT image_url FROM `" . TABLE_GALLERY . "` WHERE ID = " . $eid);
                        if (mysql_num_rows($imgRes) > 0) {
                            $imgRow = mysql_fetch_array($imgRes);
                            unlink("../../../" . $imgRow["image_url"]);
                        }
                    }
                    $data["image_url"] = $img;
                }
            }
            $success = $db->query_update(TABLE_GALLERY, $data, "ID = ".$eid);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Details updated successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:edit.php?eid=".$eid);
        break;
    case "delete" :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = $_REQUEST['eid'];
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            // Delete the gallery image first
            $imgRes = $db->query("SELECT image_url FROM `" . TABLE_GALLERY . "` WHERE ID = " . $eid);
            if (mysql_num_rows($imgRes) > 0) {
                $imgRow = mysql_fetch_array($imgRes);
                unlink("../../../" . $imgRow["image_url"]);
            }
            $success = @mysql_query("DELETE FROM `" . TABLE_GALLERY . "` WHERE ID = '{$eid}'");
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Gallery item deleted successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:index.php");
        break;
}