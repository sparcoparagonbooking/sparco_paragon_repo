<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form">
                <div class="bd_panel_head">
                    <h3>Change Password</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                            <div class="form_block">
                                <label for="oldPassword">Existing Password</label>
                                <input type="password" name="oldPassword" id="oldPassword" class="form-control2" required >
                            </div>
                            <div class="form_block">
                                <label for="newPassword" >New Password:</label>
                                <input type="password" name="newPassword" id="newPassword" class="form-control2" required>
                            </div>
                            <div class="form_block">
                                <label for="conPassword">Confirm Password:</label>
                                <input type="password" name="conPassword" id="conPassword" class="form-control2" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="UPDATE" >
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
