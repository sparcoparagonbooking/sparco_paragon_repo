<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
if (!$_REQUEST['eid']) {
    header("location:index.php");
} else {
    $editId = $_REQUEST['eid'];
}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit&eid=<?= $_REQUEST['eid']; ?>" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>Edit Cleaning Status</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <?php
                        $res = $db->query("SELECT * FROM `".TABLE_CLEANING_STATUS."` WHERE ID = ".$editId);
                        if (mysql_num_rows($res) > 0) {
                            $row = mysql_fetch_array($res);
                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                                <div class="form_block">
                                    <label>Cleaning Status <span class="valid">*</span></label>
                                    <input type="text" name="cleaningStatus" value="<?= $row['cleaning_status']; ?>" required>
                                </div>

                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
