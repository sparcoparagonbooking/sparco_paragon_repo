<?php
require("../../../config/config.inc.php");
require("../../../config/Database.class.php");
require("../../../config/Application.class.php");
if ($_SESSION['sparcoId'] == "") {
    header("location:../../logout.php");
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($optype) {
    case 'index' :
        if (!$_REQUEST["roomName"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please fill all the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $data["hotel_id"] = $App->convert($_REQUEST["hotel"]);
            $data["room_name"] = $App->convert($_REQUEST["roomName"]);
            $data["room_no"] = $App->convert($_REQUEST["roomNo"]);
            $data["room_type_id"] = $App->convert($_REQUEST["roomType"]);
            $data["rate_per_day"] = $App->convert($_REQUEST["ratePerDay"]);
            $data["tax"] = $App->convert($_REQUEST["tax"]);
            $data["total"] = $App->convert($_REQUEST["total"]);
            $data["floor_no"] = $App->convert($_REQUEST["floorNo"]);
            $data["room_status_id"] = $App->convert($_REQUEST["roomStatus"]);
            $data["cleaning_status_id"] = $App->convert($_REQUEST["cleaningStatus"]);
            $data["facilities"] = $App->convert($_REQUEST["facilities"]);
            $data["seasonal_rate"] = $App->convert($_REQUEST["seasonalRate"]);

            if ($_FILES["image"]["name"]) {
                $path_info = pathinfo($_FILES["image"]["name"]);
                $ext = $path_info["extension"];
                if (!$App->imageValidation($_FILES["image"]["name"], $_FILES["image"]["tmp_name"])) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } else {
                    $result = date("YmdHis");
                    $fileName = $result . "." . $ext;
                    $img = "";
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], "../../../images/room/" . basename($fileName))) {
                        $img = "images/room/" . $fileName;
                    }
                    $data["image_url"] = $img;
                }
            }
            $success = $db->query_insert(TABLE_ROOM, $data);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Room added successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:index.php");
        break;
    case "edit" :
        if (!$_REQUEST['eid'] || !$_REQUEST["roomName"]) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $eid = $_REQUEST['eid'];
            $success = 0;
            $data["hotel_id"] = $App->convert($_REQUEST["hotel"]);
            $data["room_name"] = $App->convert($_REQUEST["roomName"]);
            $data["room_no"] = $App->convert($_REQUEST["roomNo"]);
            $data["room_type_id"] = $App->convert($_REQUEST["roomType"]);
            $data["rate_per_day"] = $App->convert($_REQUEST["ratePerDay"]);
            $data["tax"] = $App->convert($_REQUEST["tax"]);
            $data["total"] = $App->convert($_REQUEST["total"]);
            $data["floor_no"] = $App->convert($_REQUEST["floorNo"]);
            $data["room_status_id"] = $App->convert($_REQUEST["roomStatus"]);
            $data["cleaning_status_id"] = $App->convert($_REQUEST["cleaningStatus"]);
            $data["facilities"] = $App->convert($_REQUEST["facilities"]);
            $data["seasonal_rate"] = $App->convert($_REQUEST["seasonalRate"]);

            if ($_FILES["image"]["name"]) {
                $path_info = pathinfo($_FILES["image"]["name"]);
                $ext = $path_info["extension"];
                if (!$App->imageValidation($_FILES["image"]["name"], $_FILES["image"]["tmp_name"])) {
                    $_SESSION['msg'] = $App->sessionMsgCreate("error", "Please upload a valid image!");
                } else {
                    $result = date("YmdHis");
                    $fileName = $result . "." . $ext;
                    $img = "";
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], "../../../images/room/" . basename($fileName))) {
                        $img = "images/room/" . $fileName;
                        // Deleting the existing image
                        $imgRes = mysql_query("SELECT image_url FROM `" . TABLE_ROOM . "` WHERE ID = " . $eid);
                        if (mysql_num_rows($imgRes) > 0) {
                            $imgRow = mysql_fetch_array($imgRes);
                            unlink("../../../" . $imgRow["image_url"]);
                        }
                    }
                    $data["image_url"] = $img;
                }
            }
            $success = $db->query_update(TABLE_ROOM, $data, "ID = ".$eid);
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Details updated successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:edit.php?eid=".$eid);
        break;
    case "delete" :
        if (!$_REQUEST['eid']) {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Failed to edit the details!");
        } else {
            $eid = $_REQUEST['eid'];
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            // Delete the food image first
            $imgRes = $db->query("SELECT image_url FROM `" . TABLE_ROOM . "` WHERE ID = " . $eid);
            if (mysql_num_rows($imgRes) > 0) {
                $imgRow = mysql_fetch_array($imgRes);
                unlink("../../../" . $imgRow["image_url"]);
            }
            $success = @mysql_query("DELETE FROM `" . TABLE_ROOM . "` WHERE ID = '{$eid}'");
            if ($success) {
                $_SESSION['msg'] = $App->sessionMsgCreate("success", "Room deleted successfully!");
            } else {
                $_SESSION['msg'] = $App->sessionMsgCreate("error", "Something went wrong. Please try again!");
            }
            $db->close();
        }
        header("location:index.php");
        break;
}