<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" id="room_form" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>Add Room</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Hotel <span class="valid">*</span></label>
                                <select name="hotel">
                                    <?php
                                    $hotelRes = $db->query("SELECT ".TABLE_HOTEL.".ID, ".TABLE_HOTEL.".hotel_name 
                                                              FROM `".TABLE_HOTEL."`
                                                              JOIN ".TABLE_USER." ON ".TABLE_HOTEL.".ID=".TABLE_USER.".hotel_id
                                                             WHERE ".TABLE_USER.".ID = ". $_SESSION['sparcoUserId']."");
                                    if (mysql_num_rows($hotelRes) > 0) {
                                        while ($hotelRow = mysql_fetch_array($hotelRes)) {
                                            ?>
                                            <option value="<?= $hotelRow['ID'] ?>"><?= $hotelRow['hotel_name'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Room Name <span class="valid">*</span></label>
                                <input type="text" name="roomName" data-attr="validate" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Room No <span class="valid">*</span></label>
                                <input type="text" name="roomNo" data-attr="validate" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Room Type <span class="valid">*</span></label>
                                <select name="roomType" required>
                                    <?php
                                    $roomTypeRes = $db->query("SELECT * FROM `".TABLE_ROOM_TYPE."`");
                                    if (mysql_num_rows($roomTypeRes) > 0) {
                                        while ($roomTypeRow = mysql_fetch_array($roomTypeRes)) {
                                            ?>
                                            <option value="<?= $roomTypeRow['ID'] ?>"><?= $roomTypeRow['room_type'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Rate per Day <span class="valid">*</span></label>
                                <input type="text" name="ratePerDay" data-attr="validate" required data-action="room_net" autocomplete="off" value="0" class="text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Tax Amount <span class="valid">*</span></label>
                                <input type="text" name="tax" data-attr="validate" required data-action="room_net" autocomplete="off" value="0" class="text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Total <span class="valid">*</span></label>
                                <input type="text" name="total" data-attr="validate" required readonly value="0" class="text-right">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Seasonal Rate</label>
                                <input type="text" name="seasonalRate" required value="0" class="text-right">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Floor No</label>
                                <input type="text" name="floorNo">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Room Status</label>
                                <select name="roomStatus">
                                    <?php
                                    $roomStatusRes = $db->query("SELECT * FROM `".TABLE_ROOM_STATUS."`");
                                    if (mysql_num_rows($roomStatusRes) > 0) {
                                        while ($roomStatusRow = mysql_fetch_array($roomStatusRes)) {
                                            ?>
                                            <option value="<?= $roomStatusRow['ID'] ?>"><?= $roomStatusRow['room_status'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Cleaning Status</label>
                                <select name="cleaningStatus">
                                    <?php
                                    $cleaningStatusRes = $db->query("SELECT * FROM `".TABLE_CLEANING_STATUS."`");
                                    if (mysql_num_rows($cleaningStatusRes) > 0) {
                                        while ($cleaningStatusRow = mysql_fetch_array($cleaningStatusRes)) {
                                            ?>
                                            <option value="<?= $cleaningStatusRow['ID'] ?>"><?= $cleaningStatusRow['cleaning_status'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Facilities</label>
                                <textarea name="facilities"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form_block">
                                <label>Image<span class="valid">*</span></label>
                                <input type="file" name="image" class="show_progress" id="image" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" value="SAVE">
                            <!--<button type="button" id="room_form_submit" class="bd_btn bd_btn_blue">SAVE</button>-->
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
