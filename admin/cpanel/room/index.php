<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Room</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <input type="text" name="searchName" id="" placeholder="Room name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." AND r.room_name LIKE '%".$_POST['searchName']."%'";
}

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Room Name</th>
                            <th>Room No</th>
                            <th>Hotel</th>
                            <th>Room Type</th>
                            <th>Rate</th>
                            <th>Room Status</th>
                            <th>Cleaning Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $hotelId	= $db->userHotel($_SESSION['sparcoUserId']);
                    if($_SESSION['sparcoType']== 'staff')
                    {
						$cond = $cond." AND r.hotel_id=$hotelId ";
					}
                   
                    $i=0;
                    $selectAll = "SELECT r.ID, r.room_name, r.room_no, r.rate_per_day, r.tax, r.total, r.seasonal_rate, r.floor_no, 
                                    r.facilities, r.image_url, rt.room_type, rs.room_status, cs.cleaning_status, h.hotel_name
                                    FROM `".TABLE_ROOM."` r 
                                    INNER JOIN `".TABLE_HOTEL."` h ON r.hotel_id = h.ID 
                                    INNER JOIN `".TABLE_ROOM_TYPE."` rt ON r.room_type_id = rt.ID 
                                    INNER JOIN `".TABLE_ROOM_STATUS."` rs ON r.room_status_id = rs.ID 
                                    INNER JOIN `".TABLE_CLEANING_STATUS."` cs ON r.cleaning_status_id = cs.ID 
                                    WHERE ".$cond." ";
                    //$selectAll = "SELECT * FROM ".TABLE_ROOM. " WHERE ".$cond;
                    //echo $selectAll;die;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="9" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                        $tableId = $row['ID'];
	                        ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
	                       	<td><?= $row['room_name']; ?></td>
	                        <td><?= $row['room_no']; ?></td>
	                        <td><?= $row['hotel_name']; ?></td>
	                        <td><?= $row['room_type']; ?></td>
	                        <td><?= $row['total']; ?></td>
	                        <td><?= $row['room_status']; ?></td>
	                        <td><?= $row['cleaning_status']; ?></td>
	                        <td>
	                        <a class="show_table_lnk show_table_lnk_view" data-toggle="modal" href="#room_view<?=$tableId;?>">View</a>
                                <div id="room_view<?=$tableId;?>" class="modal fade bd_modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Room Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped package_view_table">
                                                        <tbody>
                                                        <tr>
                                                            <th>Hotel</th>
                                                            <td><?= $row['hotel_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Room Name</th>
                                                            <td><?= $row['room_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Room Type</th>
                                                            <td><?= $row['room_type']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Room No</th>
                                                            <td><?= $row['room_no']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Rate Per Day</th>
                                                            <td><?= $row['rate_per_day']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Tax</th>
                                                            <td><?= $row['tax'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Total</th>
                                                            <td><?= $row['total']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Seasonal Rate</th>
                                                            <td><?= $row['seasonal_rate']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Floor No</th>
                                                            <td><?= $row['floor_no']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Room status</th>
                                                            <td><?= $row['room_status']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Cleaning status</th>
                                                            <td><?= $row['cleaning_status']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Facilities</th>
                                                            <td><?= $row['facilities']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Image</th>
                                                            <td><img style="width: 250px; height: 250px" src="../../../<?= $row['image_url']; ?>" alt="no image"/></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
	                        <a class="show_table_lnk show_table_lnk_edit" href="edit.php?eid=<?php echo $row['ID'];?>">Edit</a>
	                        <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?eid=<?php echo $row['ID'];?>&op=delete">Delete</a>
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>