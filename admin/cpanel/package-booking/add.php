<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form method="post" action="do.php?op=index" id="room_form" class="default_form" enctype="multipart/form-data">
                    <div class="bd_panel_head">
                        <h3>Package Booking</h3>
                    </div>
                    <div class="bd_panel_body">
                        <h4>Booking Details</h4>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Package <span class="valid">*</span></label>
                                    <select name="package" required="">
                                    <option value="">Select Package</option>
                                        <?php
                                        $hotelId	= $db->userHotel($_SESSION['sparcoUserId']);
                                        $roomRes = $db->query("SELECT ID, package_name FROM `".TABLE_PACKAGE."` where hotel_id= $hotelId ");
                                        if (mysql_num_rows($roomRes) > 0) {
                                            while ($roomRow = mysql_fetch_array($roomRes)) {
                                                ?>
                                                <option value="<?= $roomRow['ID'] ?>"><?= $roomRow['package_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Rate</label>
                                    <input type="text" name="rate"  >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check in date <span class="valid">*</span></label>
                                    <input type="text" class="user_date" name="check_in_date"  required autocomplete="off">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check in time </label>
                                    <input type="text" name="check_in_time"  >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check out date <span class="valid">*</span></label>
                                    <input type="text" class="user_date" name="check_out_date"  required autocomplete="off">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check out time </label>
                                    <input type="text" name="check_out_time">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Number of adult <span class="valid">*</span></label>
                                    <input type="number" name="no_adult"  required min="1">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Number of Children</label>
                                    <input type="number" name="no_child" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Number of Room <span class="valid">*</span></label>
                                    <input type="number" name="no_room" required="" min="1">
                                </div>
                            </div>
                        </div>

                        <h4>Booked by</h4>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Name<span class="valid">*</span></label>
                                    <input type="text" name="name" required="" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Address</label>
                                    <textarea name="address" ></textarea>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Pin</label>
                                    <input type="text" name="pin">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Contact Number<span class="valid">*</span></label>
                                    <input type="text" name="contact_no" required="" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Alternative number</label>
                                    <input type="text" name="alt_contact_no"  >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Email</label>
                                    <input type="email" name="email"  >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Remark</label>
                                    <textarea name="remark"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Photo</label>
                                    <input type="file" class="show_progress" name="file" />
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Document Image</label>
                                    <input type="file" class="show_progress" name="doc_file" />
                                </div>
                            </div>
                        </div>
                        <div class="multi_det_head">
                            <h3>Customer List</h3>
                            <div class="multi_det_control">
                                <ul class="page_nav">
                                    <li>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="#" id="add_customer">
                                            <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                            Add New Row
                                        </a>
                                    </li>
                                </ul>

                            </div>
                            <div class="bd_clear"></div>
                        </div>
                        <div class="multi_det_table_wrap table-responsive">
                            <table class="table table-bordered multi_det_table" id="customer_table">
                                <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>gender</th>
                                    <th>Contact No</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td><input data-field_name="name" class="customer_dynamic" type="text" name="name_1"></td>
                                        <td><input data-field_name="age" class="customer_dynamic" type="text" name="age_1"></td>
                                        <td>
                                            <select data-field_name="gender" class="customer_dynamic" name="gender_1">
                                                <option value="m">Male</option>
                                                <option value="f">Female</option>
                                            </select>
                                        </td>
                                        <td><input data-field_name="contact_no" class="customer_dynamic" type="text" name="contact_no_1"></td>
                                        <td>
                                            <a href="#" class="show_table_lnk show_table_lnk_del" data-action="del">Delete</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="bd_panel_footer">
                        <div class="panel_row">
                            <div class="form_block_full">
                                <input type="hidden" name="customer_row_count" id="customer_row_count" value="1">
                                <input type="submit" value="SAVE">
                                <!--<button type="button" id="room_form_submit" class="bd_btn bd_btn_blue">SAVE</button>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>