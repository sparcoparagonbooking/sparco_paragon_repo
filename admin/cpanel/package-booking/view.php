<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
if (!@$_REQUEST['vid']) {
    header("location:index.php");
} else {
     $viewId = mysql_real_escape_string($_REQUEST['vid']);
}

$edit 	=	"SELECT B.ID booking_id,B.check_in_date,B.check_in_time,B.check_out_date,B.check_out_time,B.room_no,
					B.booking_type,B.adult_no,B.children_no,B.room_no,B.package_id,B.rate,B.booking_type,
					B.customer_id,
					C.ID customer_id,C.name,C.address,C.pin,C.contact_no,C.alt_contact_no,C.email,C.remark,C.photo,
					C.document_img
			   FROM ".TABLE_PACKAGE_BOOKING." B,".TABLE_CUSTOMER." C
			  WHERE B.customer_id=C.ID
			    AND B.ID='$viewId'
			";
$editQry	=	$db->query_first($edit);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form method="post" action="do.php?op=index" id="room_form" class="default_form" enctype="multipart/form-data">
                    <div class="bd_panel_head">
                        <h3>Package Booking</h3>
                    </div>
                    <div class="bd_panel_body">
                        <h4>Booking Details</h4>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Package <span class="valid">*</span></label>
                                    <select name="room" disabled="">
                                    <option value="">Select Package</option>
                                        <?php
                                        $hotelId	= $db->userHotel($_SESSION['sparcoUserId']);
                                        $roomRes = $db->query("SELECT ID, package_name FROM `".TABLE_PACKAGE."` where hotel_id= ".$hotelId."");
                                        if (mysql_num_rows($roomRes) > 0) {
                                            while ($roomRow = mysql_fetch_array($roomRes)) {
                                                ?>
                                                <option value="<?= $roomRow['ID'] ?>" <?php if($editQry['package_id']==$roomRow['ID']) {echo "selected";}?>><?= $roomRow['package_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Rate <span class="valid">*</span></label>
                                    <input type="text" name="rate" value="<?= $editQry['rate'];?>" disabled="" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check in date <span class="valid">*</span></label>
                                    <input type="text" class="user_date" name="check_in_date" value="<?= $App->dbformat_date($editQry['check_in_date']);?>"  disabled="" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check in time </label>
                                    <input type="text" name="check_in_time" value="<?= $editQry['check_in_time'];?>" disabled=""  >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check out date <span class="valid">*</span></label>
                                    <input type="text" class="user_date" name="check_out_date" value="<?= $App->dbformat_date($editQry['check_out_date']);?>"  disabled="" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Check out time </label>
                                    <input type="text" name="check_out_time" value="<?= $editQry['check_out_time'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Number of adult <span class="valid">*</span></label>
                                    <input type="number" name="no_adult" value="<?= $editQry['adult_no'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Number of Children</label>
                                    <input type="number" name="no_child" value="<?= $editQry['children_no'];?>" disabled="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Number of Room <span class="valid">*</span></label>
                                    <input type="number" name="no_room" disabled="" value="<?= $editQry['room_no'];?>" >
                                </div>
                            </div>
                        </div>

                        <h4>Booked by</h4>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Name<span class="valid">*</span></label>
                                    <input type="text" name="name" value="<?= $editQry['name'];?>" disabled="" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Address</label>
                                    <textarea name="address" disabled="" ><?= $editQry['address'];?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Pin</label>
                                    <input type="text" name="pin" value="<?= $editQry['pin'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Contact Number<span class="valid">*</span></label>
                                    <input type="text" name="contact_no" value="<?= $editQry['name'];?>" disabled="" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Alternative number</label>
                                    <input type="text" value="<?= $editQry['alt_contact_no'];?>" name="alt_contact_no" disabled="" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Email</label>
                                    <input type="email" value="<?= $editQry['email'];?>" name="email" disabled="" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Remark</label>
                                    <textarea name="remark" disabled="" ><?= $editQry['remark'];?></textarea>
                                </div>
                            </div>
                        </div>

                      	<div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Photo</label>
                                    <img class="image_upload_preview" src="../../../<?= $editQry['photo'] ?>"/>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form_block">
                                    <label>Document Image</label>
                                    <img class="image_upload_preview" src="../../../<?= $editQry['document_img'] ?>"/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="multi_det_head">
                            <h3>Customer List</h3>
                            <!--<div class="multi_det_control">
                                <ul class="page_nav">
                                    <li>
                                       &nbsp;&nbsp;&nbsp;
                                        <a href="#" id="add_customer">
                                            <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                            Add New Row
                                        </a>
                                    </li>
                                </ul>
                            </div>-->
                            <div class="bd_clear"></div>
                        </div>
                        <div class="multi_det_table_wrap table-responsive">
                            <table class="table table-bordered multi_det_table" id="customer_table">
                                <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>gender</th>
                                    <th>Contact No</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $customerQry = mysql_query("SELECT * FROM ".TABLE_CUSTOMER_DETAILS." WHERE booking_id=".$editQry['booking_id']."");
                                $customerNum	=	mysql_num_rows($customerQry);
                                $i = 0;
                                if($customerNum>0)
                                {
                                while($customerRow=mysql_fetch_array($customerQry))
                                {
                                	$i++;
                                ?>
                                    <tr>
                                        <td>1</td>
                                        <td><input data-field_name="name" class="customer_dynamic" type="text" name="name_<?php= $i;?>" value="<?= $customerRow['name']; ?>" disabled=""></td>
                                        <td><input data-field_name="age" class="customer_dynamic" type="text" name="age_<?php= $i;?>" value="<?= $customerRow['age']; ?>" disabled=""></td>
                                        <td>
                                            <select data-field_name="gender" class="customer_dynamic" name="gender_<?php= $i;?>" disabled="">
                                                <option value="m" <?php if($customerRow['gender']=='m'){ echo "selected";}?>>Male</option>
                                                <option value="f" <?php if($customerRow['gender']=='f'){ echo "selected";}?>>Female</option>
                                            </select>
                                        </td>
                                        <td><input data-field_name="contact_no" class="customer_dynamic" type="text" name="contact_no_<?php= $i;?>" value="<?= $customerRow['contact_no']; ?>" disabled=""></td>
                                    </tr>
                                <?php }}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--<div class="bd_panel_footer">
                        <div class="panel_row">
                            <div class="form_block_full">
                                <input type="hidden" name="customer_row_count" id="customer_row_count" value="<?= $customerNum ;?>">
                                <input type="submit" value="SAVE">
                                <!--<button type="button" id="room_form_submit" class="bd_btn bd_btn_blue">SAVE</button>
                            </div>
                        </div>
                    </div>-->
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>