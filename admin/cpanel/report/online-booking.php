<?php
require('../admin_header.php');
if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$userId = $_SESSION['sparcoId'];
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Online Booking</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <select name="searchHotel">
                                <option value="">Hotel</option>
                                <?php
                                $hotelQry = mysql_query("SELECT  ID,hotel_name FROM  ".TABLE_HOTEL."");
                                while($hotelRow = mysql_fetch_array($hotelQry)){
                                    ?>
                                    <option value="<?= $hotelRow['ID'];?>" <?php if(@$_REQUEST['searchHotel']==$hotelRow['ID']){echo "selected";}?> ><?= $hotelRow['hotel_name'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <select name="searchCus">
                                <option value="">Customer</option>
                                <?php
                                $cusQry = mysql_query("SELECT ID,name FROM ".TABLE_CUSTOMER."");
                                while($cusRow = mysql_fetch_array($cusQry)){
                                    ?>
                                    <option value="<?= $cusRow['ID'];?>" <?php if(@$_REQUEST['searchCus']==$cusRow['ID']){echo "selected";}?>><?= $cusRow['name'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <select name="searchName">
                                <option value="">Room Type</option>
                                <?php
                                $typQry = mysql_query("SELECT ID,room_type FROM ".TABLE_ROOM_TYPE."");
                                while($typRow = mysql_fetch_array($typQry)){
                                    ?>
                                    <option value="<?= $typRow['ID'];?>" <?php if(@$_REQUEST['searchName']==$typRow['ID']){echo "selected";}?>><?= $typRow['room_type'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        	<input type="text" name="check_in_date" id="" placeholder="Check in Date" value="<?php echo @$_REQUEST['check_in_date']; ?>" class="user_date">
                        	<input type="text" name="check_out_date" id="" placeholder="Check out Date" value="<?php echo @$_REQUEST['check_out_date']; ?>" class="user_date">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@isset($_REQUEST['searchName'])&& $_REQUEST['searchName']!='')
{
    $cond	=	$cond." AND R.room_type_id = '".$_POST['searchName']."'";
}
if(@$_REQUEST['check_in_date'])
{
    $cond	=	$cond." AND B.check_in_date LIKE '%".$App->dbformat_date($_POST['check_in_date'])."%'";
}
if(@$_REQUEST['check_out_date'])
{
    $cond	=	$cond." AND B.check_out_date LIKE '%".$App->dbformat_date($_POST['check_out_date'])."%'";
}
if(@$_REQUEST['searchHotel']!='')
{
    $cond	=	$cond." AND B.hotel_id = '".$_POST['searchHotel']."'";
}
if(@$_REQUEST['searchCus']!='')
{
    $cond	=	$cond." AND B.customer_id = '".$_POST['searchCus']."'";
}
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Hotel Name</th>
                            <th>Room</th>
                            <th>Room Number</th>
                            <th>Check in Date</th>
                            <th>Check Out Date</th>
                            <th>Customer Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    $selectAll = "SELECT B.ID,B.check_in_date,B.check_out_date,B.room_no,B.booking_type,
                    					 H.hotel_name,R.room_name,C.name cusName,U.name as staffName,C.ID as cusID
                    				FROM ".TABLE_BOOKING." B
                    		  INNER JOIN ".TABLE_CUSTOMER." C ON B.customer_id=C.ID
                    		  INNER JOIN ".TABLE_HOTEL." H ON B.hotel_id=H.ID
                    		   LEFT JOIN ".TABLE_ROOM." R ON B.room_id=R.ID
                    		   LEFT JOIN ".TABLE_USER." U ON B.hotel_id = U.hotel_id
                    			   WHERE ".$cond." AND B.booking_type='online' ";
                    //echo $selectAll;die;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="8" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                        ?>
	                     <tr>
                             <td><?php echo ++$i; ?></td>
	                       	 <td><?= $row['hotel_name']; ?></td>
	                        <td><?= $row['room_name']; ?></td>
                             <td><?= $row['room_no']; ?></td>
	                        <td><?= $App->dbformat_date($row['check_in_date']); ?></td>
	                        <td><?= $App->dbformat_date($row['check_out_date']); ?></td>
	                        <td><?= $row['cusName']; ?></td>
	                        <td>
	                         <a class="show_table_lnk show_table_lnk_view" href="booking-view-more.php?vid=<?php echo $row['ID'];?>&type=online">View More</a>
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>