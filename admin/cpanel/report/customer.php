<?php
require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Customer</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                        	<input type="text" name="check_in_date" id="" placeholder="Check in Date" value="<?php echo @$_REQUEST['check_in_date']; ?>" class="user_date">
                        	<input type="text" name="check_out_date" id="" placeholder="Check out Date" value="<?php echo @$_REQUEST['check_out_date']; ?>" class="user_date">
                            <input type="text" name="searchName" id="" placeholder="Name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond	=	$cond." AND C.name LIKE '%".$_POST['searchName']."%'";
}
if(@$_REQUEST['check_in_date'])
{
	$cond	=	$cond." AND B.check_in_date LIKE '%".$App->dbformat_date($_POST['check_in_date'])."%'";
}
if(@$_REQUEST['check_out_date'])
{
	$cond	=	$cond." AND B.check_out_date LIKE '%".$App->dbformat_date($_POST['check_out_date'])."%'";
}
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Contact No</th>
                            <th>Email</th>
                            <th>Hotel</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    $selectAll = "SELECT C.ID,B.check_in_time,B.check_in_date,B.check_out_date,B.room_no,
                    					 B.booking_type,B.check_out_time,B.adult_no,B.children_no,B.rate,
                    					 B.ID bookingId,H.hotel_name,R.room_name,U.name as staffName,
                    					 C.name cusName,C.address,C.pin,C.contact_no,C.alt_contact_no,
                    					 C.email,C.remark,C.photo,C.document_img
                    				FROM ".TABLE_CUSTOMER." C 
                    		  INNER JOIN ".TABLE_BOOKING." B ON B.customer_id=C.ID
                    		  INNER JOIN ".TABLE_HOTEL." H ON B.hotel_id=H.ID
                    		   LEFT JOIN ".TABLE_ROOM." R ON B.room_id=R.ID
                    		   LEFT JOIN ".TABLE_USER." U ON B.user_id = U.ID
                    			   WHERE ".$cond."
                    			GROUP BY C.ID
                    			   ";
                    //echo $selectAll;die;
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr><td colspan="6" align="center">There is no data in list. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                        ?>
	                     <tr>                            
	                        <td><?= ++$i; ?></td>
	                       	<td><?= $row['cusName']; ?></td>
	                        <td><?= $row['address']; ?></td>
	                        <td><?= $row['contact_no']; ?></td>
	                        <td><?= $row['email']; ?></td>
	                        <td><?= $row['hotel_name']; ?></td>
	                        <td>
	                        	<a class="show_table_lnk show_table_lnk_view" href="#" data-toggle="modal" data-target="#customer_view<?= $i; ?>" >View</a>
	                        	
                        	 <div id="customer_view<?= $i; ?>" class="modal fade bd_modal" role="dialog">
                                <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Customer Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped package_view_table">
                                                        <tbody>
                                                            <tr>
                                                                <th>Customer name</th>
                                                                <td><?= $row['cusName']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Address</th>
                                                                <td><?= $row['address']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Pin</th>
                                                                <td><?= $row['pin']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Contact No</th>
                                                                <td><?= $row['contact_no']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Alternative Number</th>
                                                                <td><?= $row['alt_contact_no']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Email</th>
                                                                <td><?= $row['email'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Remark</th>
                                                                <td><?= $row['remark']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Photo</th>
                                                                <td><img  src="../../../<?= $row['photo']; ?>" alt="no image"/></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Document</th>
                                                                <td><img  src="../../../<?= $row['document_img']; ?>" alt="no image"/></td>
                                                            </tr>
                                                            <tr>
                                                            	<td colspan="2">Booking details</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Hotel</th>
                                                                <td><?= $row['hotel_name']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Room</th>
                                                                <td><?= $row['room_name']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Check in Date</th>
                                                                <td><?= $row['check_in_date']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Check in Time</th>
                                                                <td><?= $row['check_in_time']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Check out Date</th>
                                                                <td><?= $row['check_out_date']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Check out Time</th>
                                                                <td><?= $row['check_out_time']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>No of Adult</th>
                                                                <td><?= $row['adult_no']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>No of Child</th>
                                                                <td><?= $row['children_no']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>No of Room</th>
                                                                <td><?= $row['room_no']; ?></td>
                                                            </tr>
                                                             <tr>
                                                                <th>Amount</th>
                                                                <td><?= $row['rate']; ?></td>
                                                            </tr>
                                                             <tr>
                                                                <th>Booking Type</th>
                                                                <td><?= $row['booking_type']; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                             </div>
	                        </td>
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
