<?php
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 03-04-2017
 * Time: 04:05 PM
 */

require('../admin_header.php');

if($_SESSION['sparcoId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Online Payment Details</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <select name="searchHotel" >
                                <option value=" ">Hotel</option>
                                <?php $hotelQry = mysql_query("SELECT * FROM ".TABLE_HOTEL." ");
                                while($hotelRow = mysql_fetch_array($hotelQry)){
                                    ?>
                                    <option value="<?= $hotelRow['ID'];?>"><?=$hotelRow['hotel_name'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <input type="text" name="check_in_date" id="" placeholder="Check in Date" value="<?php echo @$_REQUEST['check_in_date']; ?>" class="user_date">
                            <input type="text" name="check_out_date" id="" placeholder="Check out Date" value="<?php echo @$_REQUEST['check_out_date']; ?>" class="user_date">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php
$cond="1";
if(@$_REQUEST['check_in_date'])
{
    $cond	=	$cond." AND B.check_in_date LIKE '%".$App->dbformat_date($_POST['check_in_date'])."%'";
}
if(@$_REQUEST['check_out_date'])
{
    $cond	=	$cond." AND B.check_out_date LIKE '%".$App->dbformat_date($_POST['check_out_date'])."%'";
}
if(@$_REQUEST['searchHotel'])
{
    $cond   =   $cond." AND H.ID = '".$_REQUEST['searchHotel']."'";
}
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Customer</th>
                        <th>Hotel</th>
                        <th>No.Of Rooms</th>
                        <th>Paid Amount</th>
                        <th>Received Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    $selectAll = "SELECT P.ID,P.no_of_rooms,P.paid_amount,P.received_on,P.booking_ids,
                    					 H.hotel_name,C.name cusName
                    				FROM ".TABLE_PAYMENTS." P
                    		  INNER JOIN ".TABLE_CUSTOMER." C ON P.customer_id=C.ID
                    		  INNER JOIN ".TABLE_HOTEL." H ON P.hotel_id=H.ID
                    			   WHERE ".$cond." ";

                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
                        ?>
                        <tr><td colspan="6" align="center">There is no data in list. </td></tr
                        <?php
                    }
                    else
                    {
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <tr>
                                <td><?php echo ++$i; ?></td>
                                <td><?= $row['cusName']; ?></td>
                                <td><?= $row['hotel_name']; ?></td>
                                <td><?= $row['no_of_rooms']; ?></td>
                                <td><?= $row['paid_amount']; ?></td>
                                <td><?= $App->dbformat_date($row['received_on']); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>