-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2016 at 10:41 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sparco`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

DROP TABLE IF EXISTS `tbl_booking`;
CREATE TABLE IF NOT EXISTS `tbl_booking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_date` date NOT NULL,
  `check_out_date` date NOT NULL,
  `adult_no` int(11) NOT NULL,
  `children_no` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `booking_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cleaning_status`
--

DROP TABLE IF EXISTS `tbl_cleaning_status`;
CREATE TABLE IF NOT EXISTS `tbl_cleaning_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cleaning_status` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `alt_contact_no` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `remark` text NOT NULL,
  `photo` text NOT NULL,
  `document_img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer_details`
--

DROP TABLE IF EXISTS `tbl_customer_details`;
CREATE TABLE IF NOT EXISTS `tbl_customer_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `contact_no` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_food`
--

DROP TABLE IF EXISTS `tbl_food`;
CREATE TABLE IF NOT EXISTS `tbl_food` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `food_name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

DROP TABLE IF EXISTS `tbl_gallery`;
CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hotel`
--

DROP TABLE IF EXISTS `tbl_hotel`;
CREATE TABLE IF NOT EXISTS `tbl_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

DROP TABLE IF EXISTS `tbl_login`;
CREATE TABLE IF NOT EXISTS `tbl_login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `usertype` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`ID`, `username`, `password`, `usertype`, `user_id`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

DROP TABLE IF EXISTS `tbl_package`;
CREATE TABLE IF NOT EXISTS `tbl_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(500) NOT NULL,
  `night_no` int(11) NOT NULL,
  `day_no` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `airport_transportation` tinyint(1) NOT NULL,
  `laundry_service` tinyint(1) NOT NULL,
  `internet_wifi` tinyint(1) NOT NULL,
  `break_fast` tinyint(1) NOT NULL,
  `lunch` tinyint(1) NOT NULL,
  `dinner` tinyint(1) NOT NULL,
  `candle_light_dinner` tinyint(1) NOT NULL,
  `other_inclusion` text NOT NULL,
  `exclusion` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_booking`
--

DROP TABLE IF EXISTS `tbl_package_booking`;
CREATE TABLE IF NOT EXISTS `tbl_package_booking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_date` date NOT NULL,
  `check_out_date` date NOT NULL,
  `adult_no` int(11) NOT NULL,
  `children_no` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `rate` float NOT NULL,
  `booking_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_image`
--

DROP TABLE IF EXISTS `tbl_package_image`;
CREATE TABLE IF NOT EXISTS `tbl_package_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room`
--

DROP TABLE IF EXISTS `tbl_room`;
CREATE TABLE IF NOT EXISTS `tbl_room` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `rate_per_day` float NOT NULL,
  `tax` float NOT NULL,
  `total` float NOT NULL,
  `seasonal_rate` float NOT NULL,
  `floor_no` varchar(50) NOT NULL,
  `room_status_id` int(11) NOT NULL,
  `cleaning_status_id` int(11) NOT NULL,
  `facilities` text NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_status`
--

DROP TABLE IF EXISTS `tbl_room_status`;
CREATE TABLE IF NOT EXISTS `tbl_room_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `room_status` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_type`
--

DROP TABLE IF EXISTS `tbl_room_type`;
CREATE TABLE IF NOT EXISTS `tbl_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
