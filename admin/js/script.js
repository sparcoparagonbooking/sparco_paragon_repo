/*jslint browser: true*/
/*global $, jQuery, alert*/
$(window).load(function () {
    'use strict';
    // Session Message
    $('#session_modal').modal('show');
});
$(function () {
    'use strict';
    // Global functions
    // Error message
    function triggerError(type, msg) {
        if (type == "error") {
            $('#error_pop .modal-body p').text(msg);
        }
        $('#error_pop').modal('show');
    }

    // Checking the input date is on or before current Date
    function pastDate(inputDate) {
        var checkingDate = new Date(inputDate.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
            today = new Date();
        if (checkingDate.setHours(0, 0, 0, 0) == today.setHours(0, 0, 0, 0)) {
            return "valid";
        } else if (checkingDate.setHours(0, 0, 0, 0) < today.setHours(0, 0, 0, 0)) {
            return "valid";
        } else {
            return "invalid";
        }
    }

    // Getting the difference between two dates
    function getDifferenceInDates(date1, date2) {
        var date11 = new Date(date1.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
            date22 = new Date(date2.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
            timeDiff = Math.abs(date22.getTime() - date11.getTime()),
            dayDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return dayDiff;
    }

    // Checking whole number or not
    function isWhole(inputNumber) {
        if ((!isNaN(inputNumber)) && (inputNumber % 1 == 0)) {
            return true;
        } else {
            return false;
        }
    }

    // Date picker
    if ($('.user_date').length > 0) {
        $('.user_date').each(function () {
            var curDatePicker = $(this);
            //alterTarget = curDatePicker.nextAll('.date_hidden');
            curDatePicker.datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
        });
    }

    $('.header_main_nav > li.has_child > a').click(function (e) {
        e.preventDefault();
        var targetDropMenu = $(this).next();
        $('.header_sub_nav').not(targetDropMenu).hide();
        targetDropMenu.toggle();
    });
    $('ul.side_nav > li.has_child > a').click(function (e) {
        e.preventDefault();
        var curSideNav = $(this),
            curTargetSubNav = curSideNav.nextAll('ul.side_sub_nav');
        $('ul.side_sub_nav').not(curTargetSubNav).slideUp();
        curTargetSubNav.slideToggle();
    });
    $('.side_panel').perfectScrollbar();
    $('.nav_toggler').click(function () {
        $('.side_panel').toggleClass('opened');
        $('.wrapper').toggleClass('give_way');
    });
    // Login form
    $('.login_form_block_inner input').focusin(function () {
        $(this).addClass('focus');
        $(this).prev().addClass('focus');
    });
    $('.login_form_block_inner input').focusout(function () {
        $(this).removeClass('focus');
        $(this).prev().removeClass('focus');
    });

    // Show file upload thumbnail before submitting the form
    function imageThumb(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $(input).nextAll('.image_upload_preview').remove();
            reader.onload = function (e) {
                //$('#image_thumb').attr('src', e.target.result);
                $(input).after('<img class="image_upload_preview" src="' + e.target.result + '" alt=""/>');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '.show_progress', function () {
        imageThumb($(this).get(0));
        //console.log($(this).get(0));
    });

    //live calculation in room creation
    function roomCalculation() {
        var form = $('form#room_form'),
            rate = 0,
            tax = 0,
            total = 0;
        if (form.find('input[name="ratePerDay"]').val() && !isNaN(form.find('input[name="ratePerDay"]').val())) {
            rate = parseFloat(form.find('input[name="ratePerDay"]').val());
        }
        if (form.find('input[name="tax"]').val() && !isNaN(form.find('input[name="tax"]').val())) {
            tax = parseFloat(form.find('input[name="tax"]').val());
        }
        total = (rate + tax).toFixed(2);
        form.find('input[name="total"]').val(total);
    }
    $(document).on('keyup input', 'form#room_form input[data-action="room_net"]', function () {
        roomCalculation();
    });

    /*// Validation for room form
    $(document).on('click', '#room_form_submit', function () {

    });*/
    // Updating customer row count
    function customerCountUpdator () {
        $('#customer_row_count').val($('#customer_table').find('tbody > tr').length);
    }
    // Adding multiple customer in office booking.
    $(document).on('click', '#add_customer', function (e) {
        e.preventDefault();
        var table = $('#customer_table'),
            trCount = table.find('tbody > tr').length,
            appendTr = '<tr>' +
                '<td></td>' +
                '<td><input type="text" name="name" class="customer_dynamic" data-field_name="name"></td>' +
                '<td><input type="text" name="age" class="customer_dynamic" data-field_name="age"></td>' +
                '<td>' +
                '<select name="gender" class="customer_dynamic" data-field_name="gender">' +
                '<option value="m">Male</option>' +
                '<option value="f">Female</option>' +
                '</select>' +
                '</td>' +
                '<td><input type="text" name="contact_no" class="customer_dynamic" data-field_name="contact_no"></td>' +
                '<td><a href="#" class="show_table_lnk show_table_lnk_del" data-action="del">Delete</a></td>' +
                '</tr>';
        table.find('tbody').append(appendTr);
        table.find('tbody > tr:last td:first').text(trCount + 1);
        table.find('tbody > tr:last').find('.customer_dynamic').each(function () {
            $(this).attr('name', $(this).attr('data-field_name') + '_' + (trCount + 1));
        });
        customerCountUpdator();
    });
    // Delete row of customer table
    $(document).on('click', '#customer_table > tbody > tr a[data-action="del"]', function (e) {
        e.preventDefault();
        var del = $(this),
            curTr = del.closest('tr'),
            table = del.closest('table'),
            trCount = table.find('tbody > tr').length,
            appendTr = '<tr>' +
                '<td></td>' +
                '<td><input type="text" name="name" class="customer_dynamic" data-field_name="name"></td>' +
                '<td><input type="text" name="age" class="customer_dynamic" data-field_name="age"></td>' +
                '<td>' +
                '<select name="gender" class="customer_dynamic" data-field_name="gender">' +
                '<option value="m">Male</option>' +
                '<option value="f">Female</option>' +
                '</select>' +
                '</td>' +
                '<td><input type="text" name="contact_no" class="customer_dynamic" data-field_name="contact_no"></td>' +
                '<td><a href="#" class="show_table_lnk show_table_lnk_del" data-action="del">Delete</a></td>' +
                '</tr>';
        if (trCount == 1) {
            table.find('tbody > tr').remove();
            table.find('tbody').append(appendTr);
            table.find('tbody > tr:last td:first').text('1');
            table.find('tbody > tr:last').find('.customer_dynamic').each(function () {
                $(this).attr('name', $(this).attr('data-field_name') + '_1');
            });
        } else {
            curTr.remove();
            table.find('tbody > tr').each(function () {
                var tr = $(this),
                    trIndex = tr.index();
                tr.find('td:first').text(trIndex + 1);
                tr.find('.customer_dynamic').each(function () {
                    $(this).attr('name', $(this).attr('data-field_name') + '_' + (trIndex + 1));
                });
            });
        }
        customerCountUpdator();
    });
});