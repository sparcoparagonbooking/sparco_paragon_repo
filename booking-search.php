<?php

$page_id=3.1;
include('includes/header.php');
?>
<div class="inner_banner">
    <img src="images/booking_banner.jpg" />
</div>
<?php
    include('includes/menu.php');
?>
    <div class="container">
    	<div class="inner_page_mainheading">
            <h1>Booking</h1>
            <hr>
        </div>
        <!--<h4>Showing 4 of 4 Available Hotels in Calicut</h4>-->
        <div class="row booking_inner">
            <?php
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $flag = 0;
            $adultLimit = 2;
            $kidLimit   = 2;
            $cond = 1;
            $checkInDate = date('Y-m-d');
            $checkOutDate = date('Y-m-d');
            $adultNo = 1;
            $kidNo = 0;
            if (isset($_POST['place'])) {
                $place = @mysql_escape_string($_POST['place']);
            }
            if (isset($_POST['checkInDate']) && ($_POST['checkInDate']>=date('d-m-Y'))) {
                $checkInDate = $App->dbformat_date(@mysql_escape_string($_POST['checkInDate']));
            }
            if (isset($_POST['checkOutDate']) && ($_POST['checkOutDate']>=date('d-m-Y'))) {
                $checkOutDate = $App->dbformat_date(@mysql_escape_string($_POST['checkOutDate']));
            }
            if (isset($_POST['kids_no']) && $_POST['kids_no']) {
                $kidNo = @mysql_escape_string($_POST['kids_no']);
            }
            if (isset($_POST['adult_no']) && $_POST['adult_no']) {
                $adultNo = @mysql_escape_string($_POST['adult_no']);
            }
            $roomnumber = roomNumber($adultNo,$kidNo,$adultLimit,$kidLimit); // number of rooms need for this much gust

            $roomTypeArray = array();
            $roomTypeQry = "SELECT * FROM `".TABLE_ROOM_TYPE."`";
            $roomTypeRes = mysql_query($roomTypeQry);
            if (mysql_num_rows($roomTypeRes) > 0) {
                while($roomTypeRow = mysql_fetch_array($roomTypeRes)) {
                    array_push($roomTypeArray, $roomTypeRow);
                }
            };
            $hotelArray = array();
            $hotelQry = "SELECT ID, hotel_name FROM ".TABLE_HOTEL." WHERE place='$place'";
            $hotelRes = mysql_query($hotelQry);
            if(mysql_num_rows($hotelRes)>0){
                while($hotel = mysql_fetch_array($hotelRes)){
                    $hotelSingle = array(
                        "ID" => $hotel['ID'],
                        "hotel_name" => $hotel['hotel_name'],
                        "roomTypes" => array()
                    );
                    foreach ($roomTypeArray as $roomType) {
                        $singleRoomType = array(
                            "id" => $roomType['ID'],
                            "room_type" => $roomType['room_type'],
                            "rooms" => array()
                        );
                        array_push($hotelSingle['roomTypes'], $singleRoomType);
                    }
                    array_push($hotelArray, $hotelSingle);
                }
            }
            $roomArray = array();
            foreach ($hotelArray as $hotel){
                $roomData = array();
                $hotelId = $hotel['ID'];
                $roomQry = "SELECT RM.ID , RM.room_name, RM.room_no, RM.rate_per_day, RM.image_url, H.hotel_name, H.place, RT.room_type, RM.room_type_id
                             FROM ".TABLE_ROOM." RM 
                        LEFT JOIN ".TABLE_BOOKING." BK ON RM.ID=BK.room_id
                       INNER JOIN `".TABLE_HOTEL."` H ON RM.hotel_id = H.ID 
                       INNER JOIN `".TABLE_ROOM_TYPE."` RT ON RM.room_type_id = RT.ID
                            WHERE RM.hotel_id ='$hotelId' 
                              AND ((BK.check_out_date<'$checkInDate') OR (RM.ID NOT IN(SELECT room_id FROM ".TABLE_BOOKING.")))";
                //echo $roomQry;
               $roomRes = mysql_query($roomQry);
               $roomNum = mysql_num_rows($roomRes);
               if($roomNum>=$roomnumber){
                   while($roomRow = mysql_fetch_array($roomRes)){
                       $roomData['hotelId']     = $hotel['ID'];
                       $roomData['hotel_name']  = $roomRow['hotel_name'];
                       $roomData['roomId']      = $roomRow['ID'];
                       $roomData['room_name']   = $roomRow['room_name'];
                       $roomData['room_no']     = $roomRow['room_no'];
                       $roomData['rate_per_day']= $roomRow['rate_per_day'];
                       $roomData['image_url']   = $roomRow['image_url'];
                       $roomData['place']       = $roomRow['place'];
                       $roomData['room_type']   = $roomRow['room_type'];
                       $roomData['room_type_id']   = $roomRow['room_type_id'];
                       $i = 0;
                       foreach ($hotelArray as $hotelSingle) {
                           if ($roomData['hotelId'] == $hotelSingle['ID']) {
                               $j = 0;
                               foreach ($hotelSingle['roomTypes'] as $roomType) {
                                   if ($roomType['id'] == $roomData['room_type_id']) {
                                       $flag = 1;
                                       array_push($hotelArray[$i]['roomTypes'][$j]['rooms'], $roomData);
                                   }
                                   $j += 1;
                               }
                           }
                           $i += 1;
                       }
                   }
               }
            }

            if (!empty($hotelArray)) {
                $i = 0;
                foreach ($hotelArray as $hRow) {
                    foreach ($hRow['roomTypes'] as $roomTypeRow){
                        if (sizeof($roomTypeRow['rooms'])>=$roomnumber) {
                            ?>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 booking_search_item">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding:0">
                                        <img src="<?= $roomTypeRow['rooms'][0]['image_url']; ?>"/>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" style="padding-top:10px">
                                        <form class="default_form" action="booking-payment.php" method="POST">
                                            <input type="hidden" name="rooms"
                                                   value="<?= htmlentities(serialize($roomTypeRow['rooms'])); ?>">
                                            <input type="hidden" name="numRooms" value="<?= $roomnumber; ?>">
                                            <input type="hidden" name="checkInDate" value="<?= $checkInDate ?>">
                                            <input type="hidden" name="checkOutDate" value="<?= $checkOutDate ?>">
                                            <input type="hidden" name="adult_no" value="<?= $adultNo ?>">
                                            <input type="hidden" name="kids_no" value="<?= $kidNo ?>">
                                            <h4><?= $hRow['hotel_name']; ?></h4>
                                            <p>Place : <?= $place; ?><br/>
                                                <!--Check in date : 02/10/2016<br/>
                                                Check out date : 05/10/2016<br/>-->
                                                Room type : <?= $roomTypeRow['room_type'] ?> <br/>
                                                <!--Num.of.Rooms : 4 <br/>
                                                                Num. of adults : <?= $adultNo; ?> ,
                                                                Num. of kids : <?= $kidNo; ?> <br/>-->
                                            </p>
                                            <div class="rate">
                                                <div class="left_sectn">Rate per day:
                                                    <span>&#x20B9; <?= $roomTypeRow['rooms'][0]['rate_per_day'] ?>/-</span></div>
                                                <button type="submit" class="btn btn-primary pull-right"
                                                        style="height:24px;line-height:11px;font-size:12px;" name="booking">Book Now
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i += 1;
                        if ($i == 2) {
                        $i = 0;
                        ?>
                    </div>
                    <div class="row booking_inner">
                        <?php

                        }
                    }
                }
            }
            if($flag == 0){
                ?>
                <div style="text-align: center">
                   <h4>Sorry! Search result is empty.</h4>
                </div>
                <?php
            }
            ?>
            </div>
    </div>

<?php
    function roomNumber($adultNo=1,$kidNo=0,$adultLimit,$kidLimit){
        $extraRooms = 0;
        $roomcount = ceil($adultNo/$adultLimit);
        $possibleKids = $roomcount*$kidLimit;
        if($kidNo > $possibleKids){
            $extraKids = $kidNo-$possibleKids;
            $extraRooms = ceil($extraKids/$kidLimit);
        }
        $roomcount = $roomcount+$extraRooms;
        return $roomcount;
    }
    $db->close();
	include('includes/footer.php'); 
?>