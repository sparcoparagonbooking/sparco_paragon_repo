<?php 
	$page_id=8;
	include('includes/header.php'); 
?>
    <div class="inner_banner">
        <img src="images/booking_banner.jpg" />
    </div>
	<?php 
        include('includes/menu.php'); 
    ?>
    <div class="container">
    	<div class="inner_page_mainheading">
            <h1>Contact Us</h1>
            <hr>
        </div>
        <div class="row packing_inner">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style=" height:auto;margin-top:35px; padding:0;">
                        <form id="contact_form" method="post" action="">
                            <input type="text" name="name" placeholder="Full Name *" required>
                            <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Email *"  required>
                            <input type="text" name="contact_number" pattern="[0-9]{10}" placeholder="Contact Number *" required>
                            <input type="text" name="subject" placeholder="Subject *" required />
                            <textarea name="message" placeholder="Comments"></textarea> 
                            <input class="more1" style="margin-top:0px;cursor:pointer;margin-left:10px" type="reset" name="" value="Clear" >
                            <input class="more1" style="margin-top:0px;cursor:pointer" type="submit" name="submit" value="Submit" >
                         </form>
                    </div>
					<?php   
                    if(isset($_POST["submit"]))
                    {
                        $name=$_POST['name'];
                        $email=$_POST['email'];   
                        $message=$_POST['message'];
                        $subjct=$_POST['subject'];
                        $contact=$_POST['contact_number'];                      
                        $recipient = "info@paragonbooking.com";
                        $subject = "Paragon booking: Contact Added successfully";
                        $message = "Name : .$name.\n\nEmail: $email\n\nSuject : $subjct\n\nContact Number : $contact\n\nMessage : $message";
                        $header="From:$email\r\n";
                        $header.="Reply-To:$email\r\n";
                        $header.="MIME-Version:1.0\r\n";
                        'X-Mailer: PHP/' . phpversion();
                        
                        $s=mail($recipient,$subject,$message, $header);
                        if($s)
                            {
                                echo('Your message has been sent');
                            }
                            else
                            {
                                echo('Error On Sending, Please Try Again Later');
                            }
                    }
                    ?>

                </div> 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="font-size:14px;line-height:24px;margin-top:40px;height:auto;color:#737373">
                    <div class="example1">
                        <div class="panel panel-primary">
                          <div class="panel-heading" data-acc-link="demo1">New Paragon Residency</div>
                          <div class="panel-body acc-open" data-acc-content="demo1">
                            New Paragon Residency<br />1 st Cross Road 5-B/1<br />Yeshwanthpur, Bengaluru<br />Karnataka 560022<br />India<br /><a href="tel:08023571341">08023571341</a>
                          </div>
                        </div>
        
                        <!--<div class="panel panel-primary">
                          <div class="panel-heading" data-acc-link="demo2">Paragon Hotel Thrissur</div>
                          <div class="panel-body" data-acc-content="demo2">
                            Lorem Ipsum is simply dummy<br>
                            printing and typesetting industry,<br>
                            when an unknown printer took a galley  - India.<br>
                            Tel: +91 1234567890<br>
                            Email : info@newidealceramic.com <br>
                          </div>
                        </div>
        
                        <div class="panel panel-primary">
                          <div class="panel-heading" data-acc-link="demo3">Paragon Hotel Trivandrum</div>
                          <div class="panel-body" data-acc-content="demo3">
                            Lorem Ipsum is simply dummy<br>
                            printing and typesetting industry,<br>
                            when an unknown printer took a galley  - India.<br>
                            Tel: +91 1234567890<br>
                            Email : info@newidealceramic.com <br>
                          </div>
                        </div>
        
                        <div class="panel panel-primary">
                          <div class="panel-heading" data-acc-link="demo4">
                            <h3 class="panel-title">Paragon Hotel Ernakulam</h3>
                          </div>
                          <div class="panel-body" data-acc-content="demo4">
                            Lorem Ipsum is simply dummy<br>
                            printing and typesetting industry,<br>
                            when an unknown printer took a galley  - India.<br>
                            Tel: +91 1234567890<br>
                            Email : info@newidealceramic.com <br>
                          </div>
                        </div>-->
                    </div>                
                </div>
                
            </div>
                
    
                
        </div>
        <div id="map"></div>
    </div>
    <script>
        function initMap() {
            var myLatLng = {lat: 13.020216941833496, lng: 77.5499496459961},
                contentString = '<div id="content"><h4>New Paragon Residency</h4></div>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 250
            });
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 18
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'New Paragon Residency',
                animation: google.maps.Animation.BOUNCE
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            map.addListener('idle', function () {
                infowindow.open(map, marker);
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj335zTM9brgk0tmx4Fl5CGTeLItgIZbI&callback=initMap" sync defer></script>


<?php 
	include('includes/footer.php'); 
?>